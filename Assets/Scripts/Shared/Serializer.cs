﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

/*
 * Handles serialization of arbitrary argument arrays into a byte array,
 * which can be transferred across an RPC; and the deserialization back into
 * an object array.
 */
public static class Serializer {
    // Turns the provided argument array into a byte array, to allow sending of an arbitrary
    // number of parameters over RPC. Technically also allows RPCs to send any serializable
    // data type, instead of just the types supported natively by Unity.
    public static byte[] SerializeArgs(params object[] objArgs) {
        MemoryStream stream = new MemoryStream();
        BinaryFormatter formatter = new BinaryFormatter();
        try {
            formatter.Serialize(stream, objArgs);
        } catch (SerializationException e) {
            Debug.LogError("Serialization failed: " + e.Message);
        }

        return stream.ToArray();
    }

    // Turns the serialized byte array back into an object array.
    public static object[] DeserializeArgs(byte[] args) {
        BinaryFormatter formatter = new BinaryFormatter();
        MemoryStream stream = new MemoryStream(args);
        return formatter.Deserialize(stream) as object[];
    }

}