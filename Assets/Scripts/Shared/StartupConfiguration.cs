﻿using UnityEngine;
using System.Collections;

/*
 * Startup Configuration handles the initialisation of game objects
 * and settings for the server and for clients.
 */
public class StartupConfiguration : MonoBehaviour {
    public string connectionIP = "127.0.0.1";
    public string connectionPort = "80";

    public Mothership mothershipPrefab;
    public HelmStation helmStationPrefab;
    public ScienceStation scienceStationPrefab;
    public TacticsStation tacticsStationPrefab;

    public SupportShip supportShipPrefab;
    public AcquisitionStation acquisitionStationPrefab;
    public NavigationStation navigationStationPrefeb;
    public CommunicationsStation communicationsStationPrefab;

    private bool isConnected;
    private CrewStation station;

    public int maxSupportShips;

    void Start() {
        isConnected = false;
    }

    void OnGUI() {
        if (!isConnected) {
            ConnectionMenu();
        } else {
            CrewStationSelect();
        }
    }

    private void CrewStationSelect() {
        if (!station) {
            GUI.Label(new Rect(10, 10, 200, 20), "Select Crew Station");
        
            if (GUI.Button(new Rect(10, 30, 250, 40), "Mothership Helm")) {
                networkView.RPC("AllocateShip", RPCMode.Server, (int) CrewStation.Role.Helm);
            }
            if (GUI.Button(new Rect(10, 80, 250, 40), "Mothership Science")) {
                networkView.RPC("AllocateShip", RPCMode.Server, (int) CrewStation.Role.Science);
            }
            if (GUI.Button(new Rect(10, 130, 250, 40), "Mothership Tactics")) {
                networkView.RPC("AllocateShip", RPCMode.Server, (int) CrewStation.Role.Tactics);
            }
            if (GUI.Button(new Rect(10, 180, 250, 40), "Mothership Engineering")) {
                DebugConsole.Log("Not yet implemented");
            }

            if (GUI.Button(new Rect(10, 230, 250, 40), "Support Ship Navigation")) {
                networkView.RPC("AllocateShip", RPCMode.Server, (int)CrewStation.Role.Navigation);
            }
            if (GUI.Button(new Rect(10, 280, 250, 40), "Support Ship Communications")) {
                networkView.RPC("AllocateShip", RPCMode.Server, (int) CrewStation.Role.Communications);
            }
            if (GUI.Button(new Rect(10, 330, 250, 40), "Supportship Acquisition")) {
                networkView.RPC("AllocateShip", RPCMode.Server, (int) CrewStation.Role.Acquisition);
            }
        }
    }

    private void ConnectionMenu() {
        if (Network.peerType == NetworkPeerType.Disconnected) {
            GUI.Label(new Rect(10, 10, 200, 20), "Status: Disconnected");
            connectionIP = GUI.TextField(new Rect(10, 70, 120, 40), connectionIP);
            connectionPort = GUI.TextField(new Rect(10, 110, 120, 40), connectionPort);

            if (GUI.Button(new Rect(10, 30, 120, 20), "Client Connect")) {
                Network.Connect(connectionIP, System.Convert.ToInt32(connectionPort));
            }
            if (GUI.Button(new Rect(10, 50, 120, 20), "Initialize Server")) {
                Network.InitializeServer(32, System.Convert.ToInt32(connectionPort), false);
            }
        } else if (Network.peerType == NetworkPeerType.Client) {
            DebugConsole.Log("connected as client");
            GUI.Label(new Rect(10, 10, 200, 20), "Status: Connected as Client");
            if (GUI.Button(new Rect(10, 30, 120, 20), "Disconnect")) {
                Network.Disconnect(200);
            }
        } else if (Network.peerType == NetworkPeerType.Server) {
            GUI.Label(new Rect(10, 10, 200, 20), "Status: Connected as Server");
            if (GUI.Button(new Rect(10, 30, 120, 20), "Disconnect")) {
                Network.Disconnect(200);
            }
        }
    }

    void OnServerInitialized() {
        Network.Instantiate(mothershipPrefab, new Vector3(100, 0, 100), new Quaternion(), 0);
    }

    void OnConnectedToServer() {
        isConnected = true;
    }
    
    [RPC]
    private void AllocateShip(int crewStationType, NetworkMessageInfo info) {
        Ship ship = null;
        CrewStation.Role role = (CrewStation.Role) crewStationType;
        switch (role) {
            case CrewStation.Role.Helm:
            case CrewStation.Role.Tactics:
            case CrewStation.Role.Science:
            case CrewStation.Role.Engineering:
                ship = FindMothership(role);
                break;
            case CrewStation.Role.Navigation:
            case CrewStation.Role.Communications:
            case CrewStation.Role.Acquisition:
                ship = FindSupportShipWithFreeStation(role);
                break;
        }
        if (ship) {
            networkView.RPC("AssignToShip", info.sender, ship.networkView.viewID, crewStationType);
            ship.AddStation((CrewStation.Role) crewStationType, info.sender);
        }
    }

    [RPC]
    private void AssignToShip(NetworkViewID networkViewID, int crewStationType) {
        NetworkView shipNetworkView = NetworkView.Find(networkViewID);
        Ship ship = (Ship) shipNetworkView.gameObject.GetComponent(typeof(Ship));
        ship.gameObject.AddComponent<AudioListener>();

        switch (crewStationType) {
            case (int) CrewStation.Role.Helm:
                station = (HelmStation) Instantiate(helmStationPrefab);
                break;
            case (int) CrewStation.Role.Science:
                station = (ScienceStation) Instantiate(scienceStationPrefab);
                break;
            case (int) CrewStation.Role.Tactics:
                station = (TacticsStation) Instantiate(tacticsStationPrefab);
                break;
            case (int) CrewStation.Role.Navigation:
                station = (NavigationStation)Instantiate(navigationStationPrefeb);
                break;
            case (int) CrewStation.Role.Acquisition:
                station = (AcquisitionStation) Instantiate(acquisitionStationPrefab);
                break;
            case (int) CrewStation.Role.Communications:
                station = (CommunicationsStation) Instantiate(communicationsStationPrefab);
                break;
        }

        if (station) {
            station.Allocate(ship);
        }
    }

    // Finds the Mothership and checks if the desired role is free.
    private Mothership FindMothership(CrewStation.Role role) {
        Mothership ship = NetworkView.FindObjectOfType(typeof(Mothership)) as Mothership;
        if (!ship.HasRole(role)) {
            return ship;
        } else {
            return null;
        }
    }

    // Finds all existing support ships. If any of them have the desired role free, the player is 
    // allocated to that ship. If they do not, the system checks if there is room for any more 
    // support ships. If there is room, another ship is created. If not, the assignment fails.
    private SupportShip FindSupportShipWithFreeStation(CrewStation.Role role) {
        SupportShip[] ships = NetworkView.FindObjectsOfType(typeof(SupportShip)) as SupportShip[];
        foreach (SupportShip ship in ships) {
            if (!ship.HasRole(role)) {
                return ship;
            }
        }
        if (ships.Length >= maxSupportShips) {
            return null;
        } else {
            return Network.Instantiate(supportShipPrefab, new Vector3(100, 0, 100), new Quaternion(), 0) as SupportShip;
        }
    }
}