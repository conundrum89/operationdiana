﻿using UnityEngine;
using System.Collections;

/*
 * This class is responsible for application level functionality
 * not tied specifically to gameplay features.
 */ 
public class ApplicationManager : MonoBehaviour {

    private float quitTimer;
    private bool canUpdateTimer;
    private Rect quitButton;

    public float timeBeforeQuit;
    public int targetFrameRate;

    void Awake() {
        Application.targetFrameRate = targetFrameRate;

        quitButton = new Rect(0f, 0f, Screen.width / 20, Screen.height / 20);
        quitTimer = 0;
    }

    void Update() {
        //The timer is incremented during update because on how OnGUI works in Unity
        if (canUpdateTimer) {
            quitTimer += Time.deltaTime;
            if (quitTimer >= timeBeforeQuit) {
                Application.Quit();
            }
        } else {
            quitTimer = 0;
        }
    }

    void OnGUI() {
        canUpdateTimer = GUI.RepeatButton(quitButton, "", GUIStyle.none);
    }
}
