﻿using UnityEngine;
using System.Collections;

public class DebugConsoleController : MonoBehaviour {

    public KeyCode clearText;
    public KeyCode visibilityToggle;

    void Update() {
        if (Input.GetKeyDown(clearText)) {
            DebugConsole.Clear();
        }

        if (Input.GetKeyDown(visibilityToggle)) {
            DebugConsole.isVisible = !DebugConsole.isVisible;
        }
    }
}
