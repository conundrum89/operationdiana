﻿using UnityEngine;

/* 
 * The IDropTarget interface denotes a GUI element or GameObject
 * as a valid target for swipe messages. Anything implementing this interface
 * must specify a ship and role to route the swiped message to.
 */
public interface IDropTarget {
    NetworkViewID TargetShip();
    int TargetRole();
    void RenderDropTarget();
}