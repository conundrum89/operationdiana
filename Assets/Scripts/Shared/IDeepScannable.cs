﻿using UnityEngine;

/* 
 * The IDeepScannable interface represents an object that has more detailed information
 * available to a ship holding a ShortRangeScanner. The ShortRangeScanner will delegate to
 * the FindDetailedScanTarget method when determining the Scannable to use as a scanTarget.
 */
public interface IDeepScannable {
    Scannable FindDetailedScanTarget(Vector3 mousePosition);
}