﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 *  This script keeps a collection of all the support ship instances
 *  and ensures that they don't travel too far outside the game space
 *  by causing damage if they travel too far from the centre
 */
public class ShipDistanceManager : MonoBehaviour {

    public float timeBetweenChecks = 5;
    public float distanceLimit;
    private HashSet<SupportShip> shipInstances;

    void OnServerInitialized() {
        shipInstances = new HashSet<SupportShip>();
        StartCoroutine(ShipDistanceCheck());
    }

    private IEnumerator ShipDistanceCheck() {
        UpdateShipInstances();

        foreach (SupportShip ship in shipInstances) {
            CheckDistance(ship);
        }

        yield return new WaitForSeconds(timeBetweenChecks);
        StartCoroutine(ShipDistanceCheck());
    }

    private void CheckDistance(SupportShip ship) {
        float shipDistance = Vector3.Distance(ship.transform.position, transform.position);

        if (shipDistance > distanceLimit) {
            ship.Damage();
        }
    }

    private void UpdateShipInstances() {
        //This variable will be greater when a new support ship is created between checks
        if (SupportShip.instanceCount > shipInstances.Count) {
            shipInstances.UnionWith(FindObjectsOfType(typeof(SupportShip)) as SupportShip[]);
        }
    }
}
