﻿using UnityEngine;
using System.Collections;

/*
 * Displays the less-detailed form of information about the resources on a Planet, in a form where
 * they are all dragged together, to enable sending of the entire CelestialBodyDetails in a single
 * action. This will be shown only to Science, using the LongRangeScanner.
 */
public class CelestialBodyDetailUI : DraggableUIComponent {
    private CelestialBody celestialBody;
    private Scannable scannable;

    // Display a summary of the available resources on the target celestialBody.
    // We need to use GetRelativeRect inside a DragWindow.
    protected override void RenderDraggableWindow(int windowId) {
        float top;
        Rect resourceRect;
        for (int i = 0; i < Resource.RESOURCE_TYPES.Length; i++) {
            top = 8 + (23 * i);
            resourceRect = GetRelativeRect(2, top, 96, 21, boundingRect);
            GUI.Box(resourceRect, ResourceSupply(Resource.RESOURCE_TYPES[i]));
        }

        GUI.DragWindow();
    }

    // Sends the TransferInformation message, which should be handled correctly by as many
    // stations as possible. This will prompt the receiving client to get updated information
    // about the CelestialBody from the server.
    protected override void DroppedOnTarget(IDropTarget messageTarget) {
        GetShipComponent<Communications>().SendNetworkMessage("TransferInformation", messageTarget, celestialBody.networkView.viewID);
    }
    
    protected override string WindowHeading() {
        return "Available Resources:";
    }

    public void SetCelestialBody(CelestialBody celestialBody) {
        this.celestialBody = celestialBody;
        scannable = celestialBody.GetComponent<Scannable>();
    }

    protected override bool ShouldRender() {
        return celestialBody != null && scannable.IsScanned();
    }

    private string ResourceSupply(string resourceType) {
        return resourceType + "\n" + celestialBody.GetResourceSupply(resourceType);
    }
}