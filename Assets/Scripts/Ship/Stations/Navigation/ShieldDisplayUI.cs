﻿using UnityEngine;
using System.Collections;

public class ShieldDisplayUI : UIComponent {

    private Shield ownerShield;
    private Rect powerFraction;
    private Texture2D powerFull;

    void Start() {
        ownerShield = GetShipComponent<Shield>();

        // TODO: Get some nice textures and plug them in through the editor.
        powerFull = new Texture2D(1, 1);
        powerFull.SetPixel(0, 0, ownerShield.GetColor());
        powerFull.wrapMode = TextureWrapMode.Repeat;
        powerFull.Apply();

        powerFraction = new Rect(boundingRect.xMin, boundingRect.yMin + (boundingRect.height * .2f), boundingRect.width, boundingRect.height * .8f);
    }

    public override void Render() {
        GUI.Box(boundingRect, "Shield");

        powerFraction.width = boundingRect.width * (ownerShield.GetPowerLevel() / 100.0f);
        GUI.DrawTexture(powerFraction, powerFull);
    }
}
