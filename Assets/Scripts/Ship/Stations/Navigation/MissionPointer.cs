﻿using UnityEngine;
using System.Collections;

/*
 * This script faces its target once assigned. It is used as a child component
 * of the support ship to indicate the direction of an assigned mission
 */ 
public class MissionPointer : MonoBehaviour {
    private Vector3 target;
    private bool isActive;

    void Start () {
        isActive = false;
        SetRenderer();
    }

    void Update() {
        if (isActive) {
            transform.LookAt(target);
        }
    }

    public void LookAt(Vector3 target) {
        this.target = target;
        transform.LookAt(target);

        isActive = true;
        SetRenderer();
    }

    public bool IsActive() {
        return isActive;
    }

    private void SetRenderer() {
        GetComponentInChildren<Renderer>().enabled = isActive;
    }
}