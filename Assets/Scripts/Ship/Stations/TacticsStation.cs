﻿using UnityEngine;
using System.Collections;

/*
 * The Tactics station allows the Mothership to monitor the location and status
 * of all Support Ships, and assign them missions to collect certain resources from
 * certain planets.
 */
public class TacticsStation : CrewStation {
    private const int OVERLAY_WIDTH = 20;
    private const int OVERLAY_HEIGHT = 8;

    private SystemMapUI systemMap;
    private ScanningUI scanning;
    private NotificationUI notification;

    void Start() {
        systemMap = GetComponent<SystemMapUI>();
        scanning = GetComponent<ScanningUI>();
        notification = GetComponent<NotificationUI>();

        SetStationSizes();
        SetupSupportShipOverlay();

        scanning.SetCanScan(false);
    }

    private void SetStationSizes() {
        systemMap.SetBounds(0, 0, 64, 100);
        scanning.SetBounds(64, 2, 35, 70);
        notification.SetBounds(64, 74, 35, 24);
    }

    // Adds overlays for all support ships that already exist when the Tactics station connects
    private void SetupSupportShipOverlay() {
        SupportShip[] supportShips = (SupportShip[]) NetworkView.FindObjectsOfType(typeof(SupportShip));

        foreach (SupportShip supportShip in supportShips) {
            AddNewSupportShipOverlay(supportShip);
        }
    }

    // Called directly by SupportShips in their Start function, so that support ships connecting
    // after the Tactics station will have their own overlays.
    public void AddNewSupportShipOverlay(SupportShip supportShip) {
        SupportShipOverlayUI supportShipStatus = AddSubUIComponent(typeof(SupportShipOverlayUI)) as SupportShipOverlayUI;
        supportShipStatus.SetSupportShip(supportShip);
        supportShipStatus.SetBounds(0, 0, OVERLAY_WIDTH, OVERLAY_HEIGHT, systemMap.GetBoundingRect());
    }

    // This method is called when a support ship drags their mission back to the Mothership.
    // It means that they believe their mission is complete, which should be reflected in
    // the Tactics UI. For now, we just print a Debug statement as proof that GUI DropTargets
    // work.
    public void SendMission(NetworkViewID planetNetworkID, string resourceType, NetworkViewID fromShipID) {
        Ship supportShip = NetworkView.Find(fromShipID).GetComponent<Ship>();
        Planet planet = NetworkView.Find(planetNetworkID).GetComponent<Planet>();
        Debug.Log("Mission complete: " + supportShip.name + " collected " + resourceType + " from " + planet.name);
        notification.AddNotification();
    }

    // This method is called when Science drags a scannable object's information to the
    // Tactics CrewTargetUI. It makes Tactics request the scanned information from the server -
    // which, actually, might give them more up-to-date information than Science has. Hm.
    public void TransferInformation(NetworkViewID scannableNetworkID) {
        Scannable scannable = NetworkView.Find(scannableNetworkID).GetComponent<Scannable>();
        scannable.Scan();
        notification.AddNotification();
    }
}