﻿using UnityEngine;

/*
 * The Helm Station handles the UI components and logic of the
 * things required for the Helm Station assigned to a Mothership.
 * It handles UI position and size as well as Power usage.
 */
public class HelmStation : CrewStation {
    private GalaxyMapUI galaxyMap;
    private SystemPowerUI systemPower;
    private StationStatusUI stationStatus;
    private SystemMapUI systemMap;
    private MovementControlsUI movementControls;

    private const int RIGHT_UI_FRAME_LEFT = 64;
    private const int RIGHT_UI_FRAME_WIDTH = 34;

    // TODO: Remove this and go through ship.GetComponent instead?
    private Engine engine;

    void Start() {
        galaxyMap = GetComponent<GalaxyMapUI>();
        systemPower = GetComponent<SystemPowerUI>();
        stationStatus = GetComponent<StationStatusUI>();
        systemMap = GetComponent<SystemMapUI>();
        movementControls = GetComponent<MovementControlsUI>();

        SetUI();
    }

    private void SetUI() {
        SetSystemMapUI();
        SetGalaxyMapUI();
        SetSystemPowerUI();
        SetStationStatusUI();
        SetMovementControlsUI();
    }

    public override float GetPower() {
        return (GetShip().GetComponent(typeof(Engine)) as Engine).GetFuel();
    }

    public override string GetPowerType() {
        return "Fuel";
    }

    private void SetSystemMapUI() {
        const int SYSTEM_MAP_UI_LEFT = 0;
        const int SYSTEM_MAP_UI_TOP = 0;
        const int SYSTEM_MAP_UI_WIDTH = 64;
        const int SYSTEM_MAP_UI_HEIGHT = 100;
        systemMap.SetBounds(SYSTEM_MAP_UI_LEFT, SYSTEM_MAP_UI_TOP, SYSTEM_MAP_UI_WIDTH, SYSTEM_MAP_UI_HEIGHT);
    }

    private void SetGalaxyMapUI() {
        const int GALAXY_MAP_UI_TOP = 2;
        const int GALAXY_MAP_UI_HEIGHT = 34;
        galaxyMap.SetBounds(RIGHT_UI_FRAME_LEFT, GALAXY_MAP_UI_TOP, RIGHT_UI_FRAME_WIDTH, GALAXY_MAP_UI_HEIGHT);
    }

    private void SetSystemPowerUI() {
        const int SYSTEM_POWER_UI_TOP = 38;
        const int SYSTEM_POWER_UI_HEIGHT = 7;
        systemPower.SetBounds(RIGHT_UI_FRAME_LEFT, SYSTEM_POWER_UI_TOP, RIGHT_UI_FRAME_WIDTH, SYSTEM_POWER_UI_HEIGHT);
    }

    private void SetStationStatusUI() {
        const int STATION_STATUS_UI_TOP = 47;
        const int STATION_STATUS_UI_HEIGHT = 51;
        stationStatus.SetBounds(RIGHT_UI_FRAME_LEFT, STATION_STATUS_UI_TOP, RIGHT_UI_FRAME_WIDTH, STATION_STATUS_UI_HEIGHT);
    }

    private void SetMovementControlsUI() {
        const int MOVEMENT_CONTROLS_UI_TOP = 48;
        const int MOVEMENT_CONTROLS_UI_HEIGHT = 50;
        movementControls.SetBounds(1, MOVEMENT_CONTROLS_UI_TOP, 15, MOVEMENT_CONTROLS_UI_HEIGHT);
    }
}
