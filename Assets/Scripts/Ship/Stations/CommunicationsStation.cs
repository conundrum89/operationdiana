﻿using UnityEngine;
using System.Collections;

/*
 * The Communications station facilitates communication of all kinds between the 
 * support ship and the mothership, as well as with Non-Player characters.
 */
public class CommunicationsStation : CrewStation {
    
    private NotificationUI notification;
    private MothershipUI mothership;
    private CargoHoldUI cargoHold;
    private TradeTeleporterUI teleporter;

    void Start() {
        notification = GetComponent<NotificationUI>();
        cargoHold = GetComponent<CargoHoldUI>();
        teleporter = GetComponent<TradeTeleporterUI>();
        mothership = GetComponent<MothershipUI>();

        SetStationSizes();
    }

    private void SetStationSizes() {
        //mothership.SetBounds(left,top,width,height)
        mothership.SetBounds(64, 2, 35, 70);
        notification.SetBounds(64, 74, 35, 24);
        cargoHold.SetBounds(28, 2, 35, 70);
        teleporter.SetBounds(28, 74, 35, 24);
    }

    // Sets the currentMission for the current ship, by finding the Planet associated with
    // planetViewID, and creating an empty Resource record of the type specified by resourceType.
    // We don't just use the string because later the Resource classes will have images associated
    // with them.
    public void AssignMission(NetworkViewID planetViewID, string resourceType) {
        Planet planet = NetworkView.Find(planetViewID).GetComponent(typeof(Planet)) as Planet;
        Resource resource = Resource.Instantiate(resourceType, 0);
        Communications.Mission newMission = new Communications.Mission(resource, planet);
        GetShip().GetComponent<Communications>().SetCurrentMission(newMission);
        // TODO: Should we send a notification to station when mission is updated?
        // It might be enough to just make the MissionDisplayComponent flash or something.
        // This can be covered in the UI Refinement task.
        // For now we'll play a sound, though.
        notification.AddNotification();
    }
}