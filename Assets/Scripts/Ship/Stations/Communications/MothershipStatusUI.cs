﻿using UnityEngine;
using System.Collections;

/*
 * The MothershipStatusUI is used by the Communications station to display pertinent
 * information about the Mothership, such as its health and its location. It also serves as
 * an IDropTarget for sending messages back to the Mothership's Tactics station.
 */
public class MothershipStatusUI : DraggableUIComponent, IDropTarget {
    protected override void RenderDraggableWindow(int windowId) {
        // TODO: Display actual information about the health of the mothership
        GUI.Label(GetRelativeRect(2, 20, 96, 20, boundingRect), "Hull Integrity: Maximum");
        GUI.Label(GetRelativeRect(2, 45, 96, 20, boundingRect), "Location: Sector G2");

        GUI.DragWindow();
    }

    // Don't do anything yet, but will probably be used to tell the Navigation station
    // to go to the mothership.
    protected override void DroppedOnTarget(IDropTarget messageTarget) {}

    protected override string WindowHeading() {
        return "Mothership Status";
    }

    void IDropTarget.RenderDropTarget() {
        GUI.DrawTexture(boundingRect, glow);
    }

    NetworkViewID IDropTarget.TargetShip() {
        Mothership ship = NetworkView.FindObjectOfType(typeof(Mothership)) as Mothership;
        return ship.networkView.viewID;
    }

    int IDropTarget.TargetRole() {
        return (int) CrewStation.Role.Tactics;
    }
}