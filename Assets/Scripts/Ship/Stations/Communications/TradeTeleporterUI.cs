﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

/*
 * The TradeTeleporterUI is used by the Communications station to display current resource
 * information about the resources on board the support ship. It also serves to transfer
 * resources back to the Mothership. Moves resources from CargoHold to temporary resources,
 * then on click teleports them to the Mothership.
 */
public class TradeTeleporterUI : UIComponent {
    private const string WINDOW_HEADING = "Trade Teleporter";
    public AudioClip teleportSound;
    public AudioClip cannotTeleportSound;
        
    override public void Render() {
        GUI.Box(boundingRect, "Teleporter");
        
        DrawButtons();
    }
    
    // TODO: Once CargoHoldUI is added, only teleporter DropTarget and Teleport button's required
    public void DrawButtons() {
        Rect teleportButton = GetAbsoluteRect(0, 25, 100, 50, boundingRect);
        
        if (GUI.Button(teleportButton, "Teleport Resources")) {
            TeleportResources();
        }
    }
    
    // Teleport resources to Mothership, if trade with Planet's is implemented, this function
    // should be modified to teleport to target i.e. planet, mothership etc
    private void TeleportResources() {
        List<Resource> shipResources = GetShipComponent<CargoHold>().GetResources();
        Mothership mothership = NetworkView.FindObjectOfType(typeof(Mothership)) as Mothership;
        int amount;
        
        if (shipResources.Count > 0) {
            foreach (Resource resource in shipResources) {
                amount = resource.GetAmount();
                
                if (mothership != null) {
                    ownerStation.PlayIfSilent(teleportSound);
                    mothership.GetCargoHold().AddResources(resource, amount);
                } else {
                    Debug.LogError("Mothership is NULL.");
                }
            }
            shipResources = new List<Resource>();
        } else {
            ownerStation.PlayIfSilent(cannotTeleportSound);
        }
        GetShipComponent<CargoHold>().Reset();
    }
}