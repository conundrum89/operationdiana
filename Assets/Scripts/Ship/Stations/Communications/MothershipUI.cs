﻿using UnityEngine;
using System.Collections;

/*
 * The MothershipUI displays any relevant information from the Mothership to the
 * Communications Station. This includes the "health" and location of the Mothership, as well
 * as any currently assigned mission from the Tactics station
 */
public class MothershipUI : UIComponent {
    private MothershipStatusUI mothershipStatus;
    private MissionUI mission;
    private Rect headingRect;

    // Attach the draggable MothershipStatusComponent and MissionDisplayComponent during setup.
    void Start() {
        headingRect = GetAbsoluteRect(2, 2, 96, 5, boundingRect);
        mothershipStatus = ownerStation.AddSubUIComponent(typeof(MothershipStatusUI)) as MothershipStatusUI;
        mothershipStatus.InitializeWindow(0, 2, 12, 96, 42, boundingRect);
        mission = ownerStation.AddSubUIComponent(typeof(MissionUI)) as MissionUI;
        mission.InitializeWindow(1, 2, 56, 96, 42, boundingRect);
    }

    override public void Render() {
        GUI.Box(boundingRect, "");
        GUI.Label(headingRect, "Mothership");
    }
}