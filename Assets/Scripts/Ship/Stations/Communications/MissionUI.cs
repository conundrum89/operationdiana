﻿using UnityEngine;
using System.Collections;

/*
 * The MissionUI is used by the Communications station to display the currently
 * assigned mission from the Tactics station. It is draggable to allow the mission details
 * to be sent to the Navigation and Acquisition stations, and to be marked as complete.
 */
public class MissionUI : DraggableUIComponent {
    protected override void RenderDraggableWindow(int windowId) {
        if (CurrentMission().IsInitialized()) {
            GUI.Label(GetRelativeRect(2, 20, 96, 20, boundingRect), CurrentMission().planet.name);
            GUI.Label(GetRelativeRect(2, 45, 96, 20, boundingRect), CurrentMission().resource.GetName()); 
        }

        GUI.DragWindow();
    }

    // Sends the SendMission message, which should be handled correctly by Navigation,
    // Acquisition, and possibly Tactics (to mark a mission as complete).
    protected override void DroppedOnTarget(IDropTarget messageTarget) {
        if (CurrentMission().IsInitialized()) {
            GetShipComponent<Communications>().SendNetworkMessage("SendMission", messageTarget, CurrentMission().planet.networkView.viewID, CurrentMission().resource.GetName());
        }
    }

    protected override string WindowHeading() {
        return "Current Mission:";
    }

    private Communications.Mission CurrentMission() {
        return GetShipComponent<Communications>().GetCurrentMission();
    }
}