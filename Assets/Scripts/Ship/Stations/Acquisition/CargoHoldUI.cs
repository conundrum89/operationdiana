﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;

/*
 * The CargoHoldUI displays the cargo information for a
 * support ship
 */
public class CargoHoldUI : UIComponent {
        
    override public void Render() {
        GUI.Box(boundingRect, CalculateResourceText());
    }
    
    private string CalculateResourceText() {
        List<Resource> resources = GetShipComponent<CargoHold>().GetResources();
        StringBuilder output = new StringBuilder();
        output.Append("Cargo Hold:\n");
        foreach (Resource resource in resources) {
            output.Append(resource.GetName()).Append(": ");
            output.Append(resource.GetAmount()).AppendLine();
        }
        return output.ToString();
    }
}