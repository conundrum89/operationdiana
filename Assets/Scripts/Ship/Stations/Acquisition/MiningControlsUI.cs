﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

/*
 * The MiningControlsUI class contains the UI that displays
 * the mining information for the Acquisition station.
 */
public class MiningControlsUI : UIComponent {
    private MiningLaser miningLaser;
    private Rect miningButton;

    public AudioClip miningSound;
    public AudioClip cannotMineSound;

    void Start() {
        miningButton = GetAbsoluteRect(2, 62, 96, 36, boundingRect);
        miningSound = Resources.Load("Ship/Station/Science/ScanningComplete") as AudioClip;
        cannotMineSound = Resources.Load("Ship/Station/Science/Scanning") as AudioClip;
    }

    private MiningLaser GetMiningLaser() {
        if (!miningLaser) {
            miningLaser = GetShipComponent<MiningLaser>();
        }
        return miningLaser;
    }

    override public void Render() {
        Sector sector = GetMiningLaser().GetCurrentSector();
        if (sector != null && sector.GetResource() != null) {
            GUI.Box(boundingRect, CalculateDetailText());
            if (GUI.RepeatButton(miningButton, "Mine Sector")) {
                if (GetMiningLaser().TransferResource()) {
                    ownerStation.PlayIfSilent(miningSound);
                } else {
                    ownerStation.PlayIfSilent(cannotMineSound);
                }
            }
        }
    }

    public void SetCurrentSector(Sector sector) {
        GetMiningLaser().SetCurrentSector(sector);
    }

    private string CalculateDetailText() {
        StringBuilder output = new StringBuilder();
        output.Append("Details:").AppendLine()
            .Append("Resource Available: ").Append(GetMiningLaser().GetCurrentSector().GetResource().GetName()).AppendLine()
            .Append("Resource Amount: ").Append(GetMiningLaser().GetCurrentSector().GetResource().GetAmount()).AppendLine();

        return output.ToString();
    }

    // TODO: Move all this to wherever the zoom logic goes.
    // It will probably belong in the orbiting code, or just in the station.

    /*private const int CLOSE_ZOOM_LEVEL = 50;
    private SystemMapUI systemMap;
    
    if (isCollision && !currentPlanet) {
        currentPlanet = raycastHit.transform.gameObject.GetComponent(typeof(Planet)) as Planet;
        systemMap.SetZoomLevel(CLOSE_ZOOM_LEVEL);
        currentPlanet.DrawSectors();
    } else if (!isCollision && currentPlanet) {
        currentPlanet.UndrawSectors();
        currentPlanet = null;
        systemMap.ResetZoomLevel();
    } 

    public void SetSystemMap(SystemMapUI systemMap) {
        this.systemMap = systemMap;
    }*/
    // End TODO
}