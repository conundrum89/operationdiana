﻿using UnityEngine;
using System.Collections.Generic;
using System;

/*
 * The Crew Station class is the base class for all Station entities.
 * It handles initialisation of common Station elements, for example sound,
 * as well as the assignment of Stations to Ships.
 */
public abstract class CrewStation : MonoBehaviour {
    public enum Role { Helm, Tactics, Engineering, Science, Navigation, Communications, Acquisition };
    protected NetworkPlayer player;
    private Ship ship;
    private List<UIComponent> uiComponents;

    void Awake() {
        // AudioSource is required to play sound clips. This code removes the
        // need to manually add AudioSource components to all station prefabs
        if (GetComponent<AudioSource>() == null) {
            gameObject.AddComponent<AudioSource>();
        }

        //Finds all UI Component scripts attached to the CrewStation prefab and sets them up for use
        uiComponents = new List<UIComponent>(GetComponents<UIComponent>());        
        foreach(UIComponent uiComponent in uiComponents) {
            uiComponent.SetOwnerStation(this);
        }
    }

    public void Allocate(Ship ship) {
        this.ship = ship;
        ship.SetActiveStation(this);
    }

    public void SetPlayer(NetworkPlayer player) {
        this.player = player;
    }

    public Ship GetShip() {
        return ship;
    }

    public NetworkPlayer GetPlayer() {
        return player;
    }
    
    public void AddUIComponent(UIComponent uiComponent) {
        uiComponents.Add(uiComponent);
        uiComponent.SetOwnerStation(this);
    }

    public UIComponent AddSubUIComponent(Type uiComponentType) {
        UIComponent uiComponent = gameObject.AddComponent(uiComponentType) as UIComponent;
        AddUIComponent(uiComponent);
        return uiComponent;
    }

    public List<UIComponent> GetUIComponents() {
        return uiComponents;
    }

    public void RemoveUIComponent(UIComponent uiComponent) {
        uiComponents.Remove(uiComponent);
        Destroy(uiComponent);
    }
    
    // Displays all GUI components while the station is enabled, otherwise assumes ship is respawning
    void OnGUI() {
        foreach(UIComponent uiComponent in uiComponents) {
            uiComponent.Render();
        }
    }

    // Returns 0, derived classes should override and return an actual value
    public virtual float GetPower() {
        return 0.0f; 
    }

    // Returns "Power", derived classes should override and return an actual value
    public virtual string GetPowerType() {
        return "Power";
    }

    public void PlayOverriding(AudioClip soundClip) {
        if (audio.isPlaying) {
            audio.Stop();
        }

        Play(soundClip);
    }

    public void PlayIfSilent(AudioClip soundClip) {
        if (!audio.isPlaying) {
            Play(soundClip);
        }
    }

    public void Play(AudioClip soundClip) {
        if (soundClip == null) {
            throw new UnityException("No sound assigned at " + name + " crewstation");
        }

        // The clip needs to be assigned here so that boolean checks used in 
        // PlayOverriding and PlayIfSilent functions will work
        audio.clip = soundClip;
        audio.Play();
    }

}