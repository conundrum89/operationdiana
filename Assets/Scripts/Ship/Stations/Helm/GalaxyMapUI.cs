﻿using UnityEngine;

/*
 * Displays a map of the star systems in the current galaxy,
 * and allows FTL jumping between them.
 */
public class GalaxyMapUI : UIComponent {
    override public void Render() {
        GUI.Box(boundingRect, "Galaxy Map Component");
    }
}