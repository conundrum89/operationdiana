﻿using UnityEngine;

/*
 * Displays a notification from each other station on the mothership.
 */
public class StationStatusUI : UIComponent {
    override public void Render() {
        GUI.Box(boundingRect, "Station Status Component");
    }
}