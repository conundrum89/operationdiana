using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * The Science Station handles the UI components and logic of the
 * things required for the Science Station assigned to a Mothership.
 * It handles UI position and size of components.
 */
public class ScienceStation : CrewStation {
    private SystemMapUI systemMap;
    private SystemPowerUI systemPower;
    private ScanningUI scanning;
    private NotificationUI notification;

    void Start() {
        systemMap = GetComponent<SystemMapUI>();
        systemPower = GetComponent<SystemPowerUI>();
        scanning = GetComponent<ScanningUI>();
        notification = GetComponent<NotificationUI>();

        SetStationSizes(); 
    }

    private void SetStationSizes() {
        // TODO: These values are hardcoded?
        systemMap.SetBounds(0, 0, 64, 100);
        systemPower.SetBounds(2, 2, 60, 5);
        scanning.SetBounds(64, 2, 35, 70);
        notification.SetBounds(64, 75, 35, 23);
    }

    override public float GetPower() {
        return GetShip().GetScanner().GetPowerLevel();
    }

    override public string GetPowerType() {
        return "Scanning Energy";
    }
}