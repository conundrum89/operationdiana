﻿using UnityEngine;

/*
 * Each CrewTargetUI displays on an edge of the display, and serves as a DropTarget for 
 * messages which the user wants to send to another station on the same ship. It is invisible
 * unless the user is currently dragging a DraggableUIComponent.
 */
class CrewTargetUI : UIComponent, IDropTarget {
    public enum Location { Top, Right, Bottom, Left };
    private int targetRole;
    private GUIStyle centeredStyle;

    public void SetLocation(Location location) {
        switch (location) {
            case Location.Top:
                SetBounds(20, 0, 60, 10);
                break;
            case Location.Right:
                SetBounds(90, 20, 10, 60);
                break;
            case Location.Bottom:
                SetBounds(20, 90, 60, 10);
                break;
            case Location.Left:
                SetBounds(0, 20, 10, 60);
                break;
        }
    }

    public void SetTargetRole(int targetRole) {
        this.targetRole = targetRole;
    }

    public override void Render() {
        // CrewTargetUI should not render anything while there is no DraggableUIComponent
        // being dragged.
    }

    void IDropTarget.RenderDropTarget() {
        if (centeredStyle == null) {
            centeredStyle = new GUIStyle(GUI.skin.box);
            centeredStyle.alignment = TextAnchor.MiddleCenter;
        }
        GUI.DrawTexture(boundingRect, glow);
        GUI.Box(boundingRect, ((CrewStation.Role) targetRole).ToString(), centeredStyle);
    }

    NetworkViewID IDropTarget.TargetShip() {
        return ownerStation.GetShip().networkView.viewID;
    }

    int IDropTarget.TargetRole() {
        return targetRole;
    }
}