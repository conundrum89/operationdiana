﻿using UnityEngine;

/*
 * The UI Component class is the base class for all UI display components.
 * It handles the relative sizing of the component to screen size, as well
 * as setting an owning station.
 */
abstract public class UIComponent : MonoBehaviour {
    protected CrewStation ownerStation;
    protected Rect boundingRect;
    protected Texture2D glow;

    void Awake() {
        glow = new Texture2D(1, 1);
        Color glowColor = Color.yellow;
        glowColor.a = 0.4f;
        glow.SetPixel(0, 0, glowColor);
        glow.wrapMode = TextureWrapMode.Repeat;
        glow.Apply();
    }

    abstract public void Render();

    public void SetOwnerStation(CrewStation ownerStation) {
        this.ownerStation = ownerStation;
    }

    public CrewStation GetOwnerStation() {
        return ownerStation;
    }

    // Helper function that allows you to get script components of the owner ship directly
    // T might be the CargoHold or Engine script for example
    protected T GetShipComponent<T>() where T : MonoBehaviour {
        return ownerStation.GetShip().GetComponent<T>();
    }

    // Sets the boundingRect relative to the screen size
    public virtual void SetBounds(float left, float top, float width, float height) {
        boundingRect = GetAbsoluteRect(left, top, width, height, new Rect(0, 0, Screen.width, Screen.height));
    }

    // Sets the boundingRect relative to another rect (usually a parent GUI Rect)
    public void SetBounds(float left, float top, float width, float height, Rect relativeTo) {
        boundingRect = GetAbsoluteRect(left, top, width, height, relativeTo);
    }

    // Returns a Rect with absolute coordinates, calculated as percentages and relative to rect.
    public static Rect GetAbsoluteRect(float left, float top, float width, float height, Rect rect) {
        float relativeLeftBound = rect.xMin + ((rect.width * left) / 100f);
        float relativeTopBound = rect.yMin + ((rect.height * top) / 100f);
        float relativeWidth = (rect.width * width) / 100f;
        float relativeHeight = (rect.height * height) / 100f;

        return new Rect(relativeLeftBound, relativeTopBound, relativeWidth, relativeHeight);
    }

    // Returns a Rect with relative coordinates, calculated as percentages and relative to rect.
    public static Rect GetRelativeRect(float left, float top, float width, float height, Rect rect) {
        float relativeLeftBound = (rect.width * left) / 100f;
        float relativeTopBound = (rect.height * top) / 100f;
        float relativeWidth = (rect.width * width) / 100f;
        float relativeHeight = (rect.height * height) / 100f;

        return new Rect(relativeLeftBound, relativeTopBound, relativeWidth, relativeHeight);
    }

    public Rect GetBoundingRect() {
        return boundingRect;
    }
}