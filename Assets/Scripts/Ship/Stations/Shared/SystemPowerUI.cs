﻿using UnityEngine;

/*
 * This class handles the display of power that a system has (for example, fuel amount,
 * scanning resource amount etc).
 */
public class SystemPowerUI : UIComponent {
    public Texture2D powerFull;
    public Texture2D powerEmpty;
    public Texture2D powerDrain;
    public Texture2D powerInsufficient;
    private GUIStyle centeredStyle = new GUIStyle();

    private float futureDrain = 0;

    // Defines the point to which the SystemPower would drain if some action were
    // performed. This is rendered as a yellow texture over the current power level.
    public void SetFutureDrain(float futureDrain) {
        this.futureDrain = ownerStation.GetPower() - futureDrain;
    }

    public void Start() {
        centeredStyle.alignment = TextAnchor.MiddleCenter;

        // TODO: Get some nice textures and plug them in through the editor.
        powerFull = new Texture2D(1, 1);
        powerFull.SetPixel(0, 0, Color.grey);
        powerFull.wrapMode = TextureWrapMode.Repeat;
        powerFull.Apply();

        powerEmpty = new Texture2D(1, 1);
        powerEmpty.SetPixel(0, 0, Color.blue);
        powerEmpty.wrapMode = TextureWrapMode.Repeat;
        powerEmpty.Apply();

        powerDrain = new Texture2D(1, 1);
        powerDrain.SetPixel(0, 0, Color.yellow);
        powerDrain.wrapMode = TextureWrapMode.Repeat;
        powerDrain.Apply();

        powerInsufficient = new Texture2D(1, 1);
        powerInsufficient.SetPixel(0, 0, Color.red);
        powerInsufficient.wrapMode = TextureWrapMode.Repeat;
        powerInsufficient.Apply();
    }

    // Renders the current power level as a percentage of the total power level.
    override public void Render() {
        float currentPower = ownerStation.GetPower();

        Rect powerFraction = boundingRect;
        powerFraction.width *= currentPower / 100.0f;

        GUI.DrawTexture(boundingRect, powerFull);
        GUI.DrawTexture(powerFraction, powerEmpty);

        RenderFutureDrain(powerFraction, currentPower);

        GUI.Label(boundingRect, ownerStation.GetPowerType(), centeredStyle);
    }

    // Renders a yellow overlay of the cost of the next action over the power level.
    private void RenderFutureDrain(Rect powerFraction, float currentPower) {
        if (futureDrain != 0) {
            if (futureDrain < 0) {
                GUI.DrawTexture(powerFraction, powerInsufficient);
            } else if (futureDrain < currentPower) {
                Rect drainRect = boundingRect;
                drainRect.width *= (currentPower - futureDrain) / 100.0f;
                drainRect.x += (powerFraction.width - drainRect.width);
                GUI.DrawTexture(drainRect, powerDrain);
            }
        }
    }
}