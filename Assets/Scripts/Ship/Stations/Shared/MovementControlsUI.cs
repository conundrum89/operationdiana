﻿using UnityEngine;

/*
 * Displays controls for navigating a support ship.
 */
public class MovementControlsUI : UIComponent {
    private GUIStyle sliderStyle = null;
    private Texture2D speedBarTexture = null;

    public AudioClip turningSound;
    public AudioClip speedUpSound;
    public AudioClip speedDownSound;

    // Constant values used for rendering the movement UI controls
    private const float SLIDER_MIN = -30;
    private const float SLIDER_MAX = 100;

    private Engine engine;

    private Rect sliderRect;
    private Rect turnLeftRect;
    private Rect turnRightRect;
    private Rect setToZeroRect;

    override public void Render() {
        if (!engine) {
            engine = GetShipComponent<Engine>();
        }
        if (sliderStyle == null || speedBarTexture == null) {
            SetupSliderStyle();
        }

        ControlTurningButtons();
        ControlSpeedSlider();
        DrawLabels();
    }

    public override void SetBounds(float left, float top, float width, float height) {
        base.SetBounds(left, top, width, height);
        SetupNavigationRectangles();
    }

    // Render and manage the two buttons to control the ship's desiredHeading
    private void ControlTurningButtons() {
        if (GUI.RepeatButton(turnLeftRect, "Left")) {
            ownerStation.PlayIfSilent(turningSound);
            engine.Turn(Engine.Direction.Left);
        }
        if (GUI.RepeatButton(turnRightRect, "Right")) {
            ownerStation.PlayIfSilent(turningSound);
            engine.Turn(Engine.Direction.Right);
        }
    }

    // Render and manage the slider that controls the ship's desiredSpeed
    private void ControlSpeedSlider() {
        float oldDesiredSpeed = engine.GetDesiredSpeed();
        float desiredSpeed = GUI.VerticalSlider(sliderRect, engine.GetDesiredSpeed(), SLIDER_MAX, SLIDER_MIN, sliderStyle, GUI.skin.verticalSliderThumb);

        if (GUI.Button(setToZeroRect, "Set to Zero")) {
            desiredSpeed = 0.0f;
        }

        engine.SetDesiredSpeed(desiredSpeed);

        if (desiredSpeed > oldDesiredSpeed) {
            ownerStation.PlayIfSilent(speedUpSound);
        } else if (desiredSpeed < oldDesiredSpeed) {
            ownerStation.PlayIfSilent(speedDownSound);
        }

        DrawCurrentSpeed();
    }

    private void DrawLabels() {
        GUI.Label(GetAbsoluteRect(2, 0, 100, 5, boundingRect), "Current Heading: " + engine.GetCurrentHeading().ToString("n2"));
        GUI.Label(GetAbsoluteRect(2, 5, 100, 5, boundingRect), "Desired Heading: " + engine.GetDesiredHeading().ToString("n2"));
    }

    // Render the green bar overlay to represent the ship's current speed as a portion of its desired speed
    private void DrawCurrentSpeed() {
        Rect currentSpeedRect = sliderRect;
        // Offset the bar slightly, because otherwise it doesn't line up with the texture underneath
        currentSpeedRect.x += 2;

        float currentSpeed = engine.GetSpeedInCurrentDirection();

        currentSpeedRect.height = currentSpeedRect.height * SLIDER_MAX / (float) (SLIDER_MAX - SLIDER_MIN);
        float speedDifference = (float) (SLIDER_MAX - currentSpeed) * currentSpeedRect.height / SLIDER_MAX;
        currentSpeedRect.height -= speedDifference;
        currentSpeedRect.y += speedDifference;
        GUI.DrawTexture(currentSpeedRect, speedBarTexture);
    }

    // Configure the slider so that it's visible on the black backdrop of space
    private void SetupSliderStyle() {
        sliderStyle = GUI.skin.verticalSlider;

        Texture2D sliderTexture = new Texture2D(1, 1);
        Color sliderColor = Color.white;
        sliderColor.a = 0.4f;
        sliderTexture.SetPixel(0, 0, sliderColor);
        sliderTexture.wrapMode = TextureWrapMode.Repeat;
        sliderTexture.Apply();

        sliderStyle.normal.background = sliderTexture;

        speedBarTexture = new Texture2D(1, 1);
        Color speedBarColor = Color.green;
        speedBarColor.a = 0.4f;
        speedBarTexture.SetPixel(0, 0, speedBarColor);
        speedBarTexture.wrapMode = TextureWrapMode.Repeat;
        speedBarTexture.Apply();
    }

    // Set up the rectangles used in rendering this interface
    private void SetupNavigationRectangles() {
        sliderRect = GetAbsoluteRect(46, 12, 5, 70, boundingRect);
        // Set width exactly because sliders can't be wider than this
        sliderRect.width = 7;
        turnLeftRect = GetAbsoluteRect(0, 40, 40, 20, boundingRect);
        turnRightRect = GetAbsoluteRect(60, 40, 40, 20, boundingRect);
        setToZeroRect = GetAbsoluteRect(30, 85, 40, 15, boundingRect);
    }
}