﻿using UnityEngine;
using System.Collections.Generic;

/*
 * The System Map Component handles the system map for the station.
 * It directly controls the camera and its position, centering the ship
 * that the station belongs to.
 */
public class SystemMapUI : UIComponent {
    public int defaultZoom;
    private Vector3 desiredScreenPosition;
    private HashSet<Ship> shipsToRender;

    // TODO for Elliott: currently set to Science Station level.
    // Maybe keep private but not const and update using Science calling a function?
    private const int ZOOM_OUT_LIMIT = 1000;

    void Start() {
        shipsToRender = new HashSet<Ship>();
        ResetZoomLevel();
    }

    void Update() {
        //TODO: This logic to move the camera every tick can be removed if the camera is made a child of the ship
        //this UI cares about
        Vector3 shipPosition = ownerStation.GetShip().transform.position;
        Vector3 desiredWorldPosition = Camera.main.ViewportToWorldPoint(desiredScreenPosition);

        // Always want to stay at the same height, so y is set to 0 so we don't change height
        Vector3 translateVector = new Vector3(shipPosition.x - desiredWorldPosition.x, 0f, shipPosition.z - desiredWorldPosition.z);

        Camera.main.transform.position = Camera.main.transform.position + translateVector;

        if (Ship.shipCount > shipsToRender.Count) {
            SetShipRenderingFor(Camera.main.orthographicSize);
        }
    }

    public void ResetZoomLevel() {
        SetZoomLevel(defaultZoom);
    }

    public void SetZoomLevel(float zoom) {
        Camera.main.orthographicSize = zoom;
        SetShipRenderingFor(zoom);
    }

    private void SetShipRenderingFor(float zoomLevel) {
        //Ensures the set of ships is updated to contain all current ships
        shipsToRender.UnionWith(FindObjectsOfType(typeof(Ship)) as Ship[]);
        foreach (Ship ship in shipsToRender) {
            if (zoomLevel >= ZOOM_OUT_LIMIT) {
                ship.ZoomModelSetActive(true);
            } else {
                ship.ZoomModelSetActive(false);
            }
        }
    }

    // Calculates the offset required to display the ship in the middle of the 
    // SystemMapComponent's visible area.
    override public void SetBounds(float left, float top, float width, float height) {
        base.SetBounds(left, top, width, height);
        SetDesiredScreenPosition(width);
    }

    private void SetDesiredScreenPosition(float width) {
        // The width of the system map, halved, then converted into a percentage
        float leftOffsetPercentage = width / 2 / 100f;
        // This vector is in screen space, so z is depth rather then y. 0.5 is centre on the y-axis, and the height is irrelevant because we are using an orthographic camera
        desiredScreenPosition = new Vector3(leftOffsetPercentage, 0.5f, 1);
    }

    override public void Render() { }
}