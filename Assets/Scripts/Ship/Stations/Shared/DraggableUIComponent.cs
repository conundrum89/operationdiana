﻿using UnityEngine;
using System.Collections.Generic;

/*
 * DraggableUIComponent is a subclass of UIComponent that allows the GUI elements to be dragged
 * around the screen and dropped onto other GUI elements or GameObjects as a way of transferring
 * information. A DraggableUIComponent should never be attached to a gameObject directly in the
 * editor - instead, it should be added by the parent UIComponent, by calling:
 * DraggableClass myDrag = ownerStation.AddDraggableUIComponent(typeof(DraggableClass)) as DraggableClass;
 * Once added, the ownerStation will handle rendering of the DraggableUIComponent - the parent UIComponent
 * does *not* need to call draggableComponent.Render() manually.
 */
public abstract class DraggableUIComponent : UIComponent {
    protected Rect windowRect;
    protected int windowId;

    void Update() {
        if (Input.GetMouseButtonUp(0) && IsDragged()) {
            OnDrop();
        }
    }

    // ShouldRender() always returns true, unless overridden by a subclass.
    override public void Render() {
        // Sometimes we try to render before ownerStation is set.
        if (ownerStation != null && ShouldRender()) {
            windowRect = GUI.Window(windowId, windowRect, RenderDraggableWindow, WindowHeading());
        }
        // If we're being dragged, make the IDropTargets highlight themselves
        if (IsDragged()) {
            // First find the UIComponent IDropTargets on the current station
            Component[] dropTargets = gameObject.GetComponents(typeof(IDropTarget));
            foreach (IDropTarget dropTarget in dropTargets) {
                dropTarget.RenderDropTarget();
            }
            // TODO: Find the gameObject IDropTargets.
            // This may end up actually being sidestepped by using UI DropTargets for gameObjects
        }
    }

    // Called when the DraggableUIComponent is dropped
    // (mouse button is released while dragging)
    private void OnDrop() {
        IDropTarget messageTarget = FindGUIDropMessageTarget() ?? FindGameObjectDropMessageTarget();
        if (messageTarget != null) {
            DroppedOnTarget(messageTarget);
        }
        // Reset DragWindow to default position.
        windowRect = boundingRect;
    }

    // Check all registered IDropTarget GUI elements to see whether the mouse is
    // within any of them
    protected IDropTarget FindGUIDropMessageTarget() {
        IDropTarget dropTarget;
        // Correct mousePosition to use the correct coordinate system.
        Vector2 correctedMousePosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        List<UIComponent> uiComponents = ownerStation.GetUIComponents();
        foreach (UIComponent uiComponent in uiComponents) {
            if ((dropTarget = uiComponent as IDropTarget) != null
                && uiComponent.GetBoundingRect().Contains(correctedMousePosition)) {
                    return dropTarget;
            }
        }
        return null;
    }
    
    // Cast a ray to check for a valid IDropTarget under the mouse position
    protected IDropTarget FindGameObjectDropMessageTarget() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo)) {
            return hitInfo.collider.gameObject.GetComponent(typeof(IDropTarget)) as IDropTarget;
        }
        return null;
    }

    // If the DragWindow isn't in it's default position, it must be being dragged.
    protected bool IsDragged() {
        if (windowRect != boundingRect) {
            return true;
        }
        return false;
    }

    // Each DragWindow on the screen needs a unique ID
    public void InitializeWindow(int id, float left, float top, float width, float height, Rect parentRect) {
        windowId = id;
        SetBounds(left, top, width, height, parentRect);
        windowRect = boundingRect;
    }

    // Defines the contents of the draggable window. The only argument is a unique index for the window.
    // NOTE: Overriding methods must include the GUI.DragWindow() call at the end of the RenderDraggableWindow method.
    protected abstract void RenderDraggableWindow(int windowId);
    // Specifies a string to be rendered in the "heading" area of the draggable window.
    protected abstract string WindowHeading();
    // Specifies what to do when the draggable window is dropped onto a valid IDropTarget.
    protected abstract void DroppedOnTarget(IDropTarget messageTarget);
    // Can be overriden to define when a DraggableWindow should not be rendered. For an example, see
    // ResourceDetailComponent.cs
    protected virtual bool ShouldRender() {
        return true;
    }
}