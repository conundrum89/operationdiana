﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

/*
 * The ScanningUI is used by any station which can view scanned details about a scannable object.
 * It is left up to the scanner on the ship to perform the actual scanning logic, and it is up
 * to the scannable object to determine how it is displayed. This class handles the display
 * of scanning progress, and the retrieval of the scanned information display from the scannable
 * object.
 */
public class ScanningUI : UIComponent {
    private Scanner scanner;

    private Rect headingRect;
    private Rect scanButtonRect;
    private Rect scanProgressRect;
    private Rect detailRect;
    private List<UIComponent> subUIComponents = new List<UIComponent>();
    private SystemPowerUI systemPower;

    public AudioClip scanningSound;
    public AudioClip scanCompleteSound;

    private Texture2D scanFull;
    private Texture2D scanEmpty;
    private GUIStyle centeredStyle = new GUIStyle();

    private bool canScan = true;

    private const string NOT_SCANNED = "Not yet scanned.";
    private const string SCANNING = "Scanning...";
    private const string OUT_OF_RANGE = "Cannot Scan - Out of range";

    void Start() {
        headingRect = GetAbsoluteRect(2, 2, 96, 5, boundingRect);
        scanButtonRect = GetAbsoluteRect(20, 9, 60, 9, boundingRect);
        scanProgressRect = GetAbsoluteRect(2, 9, 96, 9, boundingRect);
        detailRect = GetAbsoluteRect(2, 21, 96, 77, boundingRect);

        scanner = ownerStation.GetShip().GetScanner();
        systemPower = ownerStation.GetComponent<SystemPowerUI>();

        centeredStyle.alignment = TextAnchor.MiddleCenter;

        // TODO: Get some nice textures and plug them in through the editor.
        scanFull = new Texture2D(1, 1);
        scanFull.SetPixel(0, 0, Color.grey);
        scanFull.wrapMode = TextureWrapMode.Repeat;
        scanFull.Apply();

        scanEmpty = new Texture2D(1, 1);
        scanEmpty.SetPixel(0, 0, Color.green);
        scanEmpty.wrapMode = TextureWrapMode.Repeat;
        scanEmpty.Apply();
    }

    void Update() {
        // Don't let the user change planet target while scanning
        if (Input.GetMouseButtonDown(0) && MouseOverMap() && !scanner.IsScanning()) {
            SetScanTarget(scanner.FindScanTarget());
        }
    }

    private bool MouseOverMap() {
        Vector2 correctedMousePosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        return GetComponent<SystemMapUI>().GetBoundingRect().Contains(correctedMousePosition);
    }

    // Determines whether to display the scan controls, and whether the current target has been scanned.
    public override void Render() {
        Scannable scanTarget = scanner.GetScanTarget();

        GUI.Box(boundingRect, "");
        if (scanTarget != null) {
            GUI.Label(headingRect, scanTarget.WindowHeading());
            if (canScan) {
                RenderScanControls(scanTarget);
            }
            if (scanTarget.IsScanned()) {
                GUILayout.BeginArea(detailRect);
                scanTarget.RenderDetail();
                GUILayout.EndArea();
            } else {
                GUI.Label(detailRect, NOT_SCANNED);
            }
            
        }
    }
    
    // Renders the Scan button if not currently scanning, or the progress bar if a scan is underway.
    private void RenderScanControls(Scannable scanTarget) {
        if (scanner.IsScanning()) {
            RenderScanProgressBar();
        } else if (scanTarget.CanBeScannedBy(ownerStation.GetShip())) {
            if (GUI.Button(scanButtonRect, "Scan")) {
                ownerStation.PlayOverriding(scanningSound);
                scanner.Activate();
            }
        } else {
            GUI.Box(scanButtonRect, OUT_OF_RANGE);
        }
    }

    private void RenderScanProgressBar() {
        Rect progressFraction = scanProgressRect;
        progressFraction.width *= scanner.GetCurrentScanProgress() / 100.0f;

        GUI.DrawTexture(scanProgressRect, scanFull);
        GUI.DrawTexture(progressFraction, scanEmpty);

        GUI.Label(scanProgressRect, SCANNING, centeredStyle);
    }
    
    // Clears the previous target, then replaces it with the new UI elements to be rendered
    // from the new target. Also sets the future drain cost of the current scantarget.
    private void SetScanTarget(Scannable scanTarget) {
        UnselectPreviousTarget();
        if (scanTarget != null) {
            subUIComponents = scanTarget.GetSubUIComponents(this);
            if (systemPower) {
                systemPower.SetFutureDrain(scanner.GetScanCost());
            }
        } else if (systemPower) {
            systemPower.SetFutureDrain(0);
        }
    }

    private void UnselectPreviousTarget() {
        foreach (UIComponent subUIComponent in subUIComponents) {
            ownerStation.RemoveUIComponent(subUIComponent);
        }
        subUIComponents.Clear();
    }

    public void SetCanScan(bool canScan) {
        this.canScan = canScan;
    }

    public void ScanComplete() {
        ownerStation.PlayOverriding(scanCompleteSound);
    }
}