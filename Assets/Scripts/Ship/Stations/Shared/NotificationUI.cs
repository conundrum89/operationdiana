﻿using UnityEngine;
using System.Collections;

/* 
 * Displays a list of Notifications received by the current station.
 * Each notification is a DraggableUIComponent, to allow them to be 
 * swiped to other stations or dismissed.
 */
public class NotificationUI : UIComponent {
    public AudioClip notificationReceivedSound;
    // private List<Notification> notifications;

    override public void Render() {
        GUI.Box(boundingRect, "Notification Component");
    }

    // For now just plays a nice sound, but will eventually handle adding a
    // new Notification to the list of Notifications and displaying it.
    public void AddNotification() {
        ownerStation.PlayOverriding(notificationReceivedSound);
    }
}