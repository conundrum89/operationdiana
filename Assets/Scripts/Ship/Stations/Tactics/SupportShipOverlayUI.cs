﻿using UnityEngine;
using System.Collections;

/*
 * One SupportShipOverlayUI is created on the TacticsStation for each Support ship. This allows
 * Tactics to easily drop missions onto support ships and also identify them by name. All information
 * is bound to display within the SystemMapUI so as not to overlay other UI components.
 */
public class SupportShipOverlayUI : UIComponent, IDropTarget {
    private Rect systemMapRect;
    private SupportShip supportShip;
    private string shipName;
    private Vector3 shipPositionOnScreen;

    void Start() {
        if (!ownerStation.GetComponent<SystemMapUI>()) {
            throw new MissingComponentException("The station using SupportShipOverlay must have a SystemMapUI");
        }
        systemMapRect = ownerStation.GetComponent<SystemMapUI>().GetBoundingRect();
    }

    // Calculates ship position in screen coordinates, then renders the relevant information inside the box
    // if the support ship is within Tactics' field of view.
    public override void Render() {
        Vector3 shipPosition = supportShip.transform.position;
        shipPositionOnScreen = Camera.main.WorldToScreenPoint(shipPosition);
        if (systemMapRect.Contains(shipPositionOnScreen)) {
            CalculateOverlayLocation();

            GUI.Box(boundingRect, "");
            GUILayout.BeginArea(boundingRect);
            GUILayout.Label(shipName);
            GUILayout.Label("Location: [x: " + (int) shipPosition.x / 10 + ", y: " + (int) shipPosition.z / 10 + "]");
            GUILayout.EndArea();
        }
    }

    public void SetSupportShip(SupportShip ship) {
        supportShip = ship;
        shipName = supportShip.name;
    }

    // Calculates where the overlay should display - ensuring it does not fall outside the bounds
    // of the SystemMapUI.
    private void CalculateOverlayLocation() {
        boundingRect.x = shipPositionOnScreen.x - (boundingRect.width / 2.0f);

        // Checks the rectangle against the left and then the right edge of the systemMapUI
        if (boundingRect.x < 0) {
            boundingRect.x = 0;
        } else if (boundingRect.x + boundingRect.width > systemMapRect.xMax) {
            boundingRect.x = systemMapRect.xMax - boundingRect.width;
        }

        boundingRect.y = Screen.height - shipPositionOnScreen.y - boundingRect.height;

        // Checks the rectangle against the top and then bottom edge of the systemMapUI
        if (boundingRect.y < 0) {
            boundingRect.y = 0;
        } else if (boundingRect.y + boundingRect.height > systemMapRect.yMax) {
            boundingRect.y = systemMapRect.yMax - boundingRect.height;
        }
    }

    void IDropTarget.RenderDropTarget() {
        GUI.DrawTexture(boundingRect, glow);
    }

    NetworkViewID IDropTarget.TargetShip() {
        return supportShip.networkView.viewID;
    }

    int IDropTarget.TargetRole() {
        return (int) CrewStation.Role.Communications;
    }
}