﻿using UnityEngine;
using System.Collections;

/*
 * Displays the less-detailed form of information about the resources on a Planet, in a form where
 * each can be dragged individually to enable sending of Planet/Resource combination as a Mission. 
 * This will be shown only to Tactics, using the LongRangeScanner.
 */
public class ResourceDetailUI : DraggableUIComponent {
    private string resourceType;
    private CelestialBody celestialBody;
    private Scannable scannable;

    // Display the supply levels of the specified resourceType on the current planet.
    // We need to use GetRelativeRect inside a DragWindow.
    protected override void RenderDraggableWindow(int windowID) {
        GUI.Label(GetRelativeRect(2, 30, 96, 50, boundingRect), ResourceSupply());
        GUI.DragWindow();
    }

    // Sends the AssignMission message, which should be handled correctly by Communications.
    // Should probably be handled by Tactics as well, when coming from Science.
    protected override void DroppedOnTarget(IDropTarget messageTarget) {
        GetShipComponent<Communications>().SendNetworkMessage("AssignMission", messageTarget, celestialBody.networkView.viewID, resourceType);
    }

    protected override string WindowHeading() {
        return resourceType;
    }

    public void SetCelestialBody(CelestialBody celestialBody) {
        this.celestialBody = celestialBody;
        scannable = celestialBody.GetComponent<Scannable>();
    }

    public void SetResource(string resourceType) {
        this.resourceType = resourceType;
    }

    protected override bool ShouldRender() {
        return celestialBody != null && scannable.IsScanned();
    }

    private string ResourceSupply() {
        return celestialBody.GetResourceSupply(resourceType);
    }
}