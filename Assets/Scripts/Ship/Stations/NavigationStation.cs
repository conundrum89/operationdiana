﻿using UnityEngine;
using System.Collections;

public class NavigationStation : CrewStation {
    private SystemMapUI systemMap;
    private NotificationUI notification;
    private ScanningUI scanning;
    private MovementControlsUI movementControls;
    private ShieldDisplayUI shieldDisplay;

    private Communications communications;
    private MissionPointer missionPointer;

    private const int RIGHT_UI_FRAME_LEFT = 64;
    private const int RIGHT_UI_FRAME_WIDTH = 34;

    void Start() {
        systemMap = GetComponent<SystemMapUI>();
        notification = GetComponent<NotificationUI>();
        scanning = GetComponent<ScanningUI>();
        movementControls = GetComponent<MovementControlsUI>();
        shieldDisplay = GetComponent<ShieldDisplayUI>();

        scanning.SetCanScan(false);
        communications = GetShip().GetComponent<Communications>();
        missionPointer = GetShip().GetComponentInChildren<MissionPointer>();

        SetUI();
    }

    void Update() {
        if (communications.GetCurrentMission().IsInitialized() && !missionPointer.IsActive()) {
            missionPointer.LookAt(communications.GetCurrentMission().planet.transform.position);
        }
    }

    private void SetUI() {
        SetMovementControlsUI();
        SetScanningUI();
        SetNotificationUI();
        SetSystemMapUI();
        SetShieldDisplayUI();
    }

    private void SetShieldDisplayUI() {
        const int SHIELD_UI_TOP = 54;
        const int SHIELD_UI_HEIGHT = 19;
        shieldDisplay.SetBounds(RIGHT_UI_FRAME_LEFT, SHIELD_UI_TOP, RIGHT_UI_FRAME_WIDTH, SHIELD_UI_HEIGHT);
    }

    private void SetSystemMapUI() {
        const int SYSTEM_MAP_UI_LEFT = 0;
        const int SYSTEM_MAP_UI_TOP = 0;
        const int SYSTEM_MAP_UI_WIDTH = 64;
        const int SYSTEM_MAP_UI_HEIGHT = 100;
        systemMap.SetBounds(SYSTEM_MAP_UI_LEFT, SYSTEM_MAP_UI_TOP, SYSTEM_MAP_UI_WIDTH, SYSTEM_MAP_UI_HEIGHT);
    }

    private void SetScanningUI() {
        const int DETAIL_UI_TOP = 2;
        const int DETAIL_UI_HEIGHT = 50;
        scanning.SetBounds(RIGHT_UI_FRAME_LEFT, DETAIL_UI_TOP, RIGHT_UI_FRAME_WIDTH, DETAIL_UI_HEIGHT);
    }

    private void SetNotificationUI() {
        const int NOTIFICATION_UI_TOP = 75;
        const int NOTIFICATION_UI_HEIGHT = 23;
        notification.SetBounds(RIGHT_UI_FRAME_LEFT, NOTIFICATION_UI_TOP, RIGHT_UI_FRAME_WIDTH, NOTIFICATION_UI_HEIGHT);
    }

    private void SetMovementControlsUI() {
        const int MOVEMENT_CONTROLS_UI_TOP = 48;
        const int MOVEMENT_CONTROLS_UI_HEIGHT = 50;
        movementControls.SetBounds(1, MOVEMENT_CONTROLS_UI_TOP, 15, MOVEMENT_CONTROLS_UI_HEIGHT);
    }

    // This method is called when Communications drags their mission to Navigation.
    // It will set the current mission on the local ship, so that an indicator can be displayed
    // to point the way to the destination planet.
    public void SendMission(NetworkViewID planetNetworkID, string resourceType, NetworkViewID fromShipID) {
        Planet planet = NetworkView.Find(planetNetworkID).GetComponent<Planet>();
        Resource resource = Resource.Instantiate(resourceType, 0);
        Communications.Mission newMission = new Communications.Mission(resource, planet);
        GetShip().GetComponent<Communications>().SetCurrentMission(newMission);
        notification.AddNotification();
    }
}
