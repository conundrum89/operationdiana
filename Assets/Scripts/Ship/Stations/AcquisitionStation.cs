﻿using UnityEngine;
using System.Collections;

/*
 * The Acquisition station holds the mining components for the support ship
 * Acquisition station. It handles the size and positioning of the UI components
 * for the station.
 */
public class AcquisitionStation : CrewStation {

    private ScanningUI miningControls;
    private SystemMapUI systemMap;
    private CargoHoldUI cargoHold;
    private NotificationUI notification;

    void Start() {
        miningControls = GetComponent<ScanningUI>();
        systemMap = GetComponent<SystemMapUI>();
        cargoHold = GetComponent<CargoHoldUI>();
        notification = GetComponent<NotificationUI>();

        SetStationSizes();
    }

    private void SetStationSizes() {
        systemMap.SetBounds(0, 0, 64, 100);
        miningControls.SetBounds(64, 2, 35, 58);
        cargoHold.SetBounds(64, 62, 35, 20);
        notification.SetBounds(64, 84, 35, 14);
    }
}