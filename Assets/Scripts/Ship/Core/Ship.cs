﻿using UnityEngine;
using System.Collections.Generic;

/*
 * This is the base class for all ships in the game. It handles common components 
 * and stores the players that have been assigned to the ship.
 */
public abstract class Ship : MonoBehaviour {
    private Dictionary<CrewStation.Role, NetworkPlayer> players = new Dictionary<CrewStation.Role, NetworkPlayer>();
    private CrewStation activeStation;
    
    protected CargoHold cargoHold;
    protected Engine engine;
    // Scanner is public because it needs to be set to a subclass, not just to "Scanner"
    public Scanner scanner;

    public Transform mapZoomModel;
    public static int shipCount = 0;
    
    void Awake() {
        engine = GetComponent<Engine>();
        cargoHold = GetComponent<CargoHold>();
        
        shipCount++;
    }
	
    public Scanner GetScanner() {
        return scanner;
    }

    public void AddStation(CrewStation.Role role, NetworkPlayer player) {
        foreach (NetworkPlayer existingCrew in players.Values) {
            networkView.RPC("AddCrewTarget", existingCrew, (int) role);
        }
        foreach (CrewStation.Role existingRole in players.Keys) {
            networkView.RPC("AddCrewTarget", player, (int) existingRole);
        }
        players.Add(role, player);
    }
    
    public CargoHold GetCargoHold(){
        return cargoHold;
    }

    public void SetActiveStation(CrewStation station) {
        activeStation = station;
    }

    public CrewStation GetActiveStation() {
        return activeStation;
    }

    public bool HasRole(CrewStation.Role role) {
        return players.ContainsKey(role);
    }

    // Checks the players Dictionary for a player on the current ship with
    // the desired role. Returns null if no player is found.
    public NetworkPlayer GetPlayerWithRole(CrewStation.Role role) {
        NetworkPlayer player;
        players.TryGetValue(role, out player);
        return player;
    }

    // Adds a CrewTargetUI for the specified role to the active station.
    // This RPC runs on the client.
    [RPC]
    public void AddCrewTarget(int role) {
        CrewTargetUI crewTarget = GetActiveStation().gameObject.AddComponent<CrewTargetUI>();
        crewTarget.SetTargetRole(role);
        // Use the role to go clockwise around the screen
        crewTarget.SetLocation((CrewTargetUI.Location) (role % 4));
        GetActiveStation().AddUIComponent(crewTarget);
    }

    public void ZoomModelSetActive(bool status) {
        if (mapZoomModel.renderer.enabled != status) {
            mapZoomModel.renderer.enabled = status;
        }
    }
}
