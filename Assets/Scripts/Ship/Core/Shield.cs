﻿using UnityEngine;
using System.Collections;

/*
 *  This class stores a shield level for the support ship and controls visual effects when it is activated.
 *  This can be affected by damage in combat and collisions with celestial bodies
 */
public class Shield : MonoBehaviour {
    public float shieldEffectDuration;
    public Transform shieldVisual;
    public AudioClip shieldActive;

    private const float STARTING_POWER_LEVEL = 100;
    private const int DECREMENT_AMOUNT = 10;
    
    private float powerLevel;
    private Color shieldActiveColor;
    private Color shieldPassiveColor;
    private Ship owner;

    void Start() {
        powerLevel = STARTING_POWER_LEVEL;

        shieldPassiveColor = shieldVisual.renderer.material.color;
        shieldActiveColor = shieldPassiveColor;
        shieldActiveColor.a = .5f;

        owner = GetComponent<Ship>();
    }

    public float Damage() {
        powerLevel -= DECREMENT_AMOUNT;
        networkView.RPC("ActivateShield", RPCMode.Others);
        networkView.RPC("UpdatePowerLevel", RPCMode.Others, powerLevel);
        return powerLevel;
    }

    [RPC]
    private void ActivateShield() {
        if (owner.GetActiveStation()) {
            owner.GetActiveStation().PlayOverriding(shieldActive);
        }
        StartCoroutine(ShieldEffect());
    }

    [RPC]
    private void UpdatePowerLevel(float powerLevel) {
        this.powerLevel = powerLevel;
    }

    private IEnumerator ShieldEffect() {
        float timeElapsed = 0;

        while (timeElapsed < shieldEffectDuration) {
            shieldVisual.renderer.material.color = Color.Lerp(shieldActiveColor, shieldPassiveColor, timeElapsed / shieldEffectDuration);

            timeElapsed += Time.deltaTime;
            yield return null;
        }

        shieldVisual.renderer.material.color = shieldPassiveColor;
    }

    public float GetPowerLevel() {
        return powerLevel;
    }

    public void Reset() {
        powerLevel = STARTING_POWER_LEVEL;
        networkView.RPC("UpdatePowerLevel", RPCMode.Others, powerLevel);
    }

    public Color GetColor() {
        return shieldActiveColor;
    }
}
