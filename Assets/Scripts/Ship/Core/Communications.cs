﻿using UnityEngine;
using System.Collections.Generic;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System;
using System.Reflection;

/*
 * The Communications class facilitates communication between players. Any station wishing to 
 * send a message to another station or ship does so through this class, which then handles
 * finding the appropriate NetworkPlayer, and transforming the message into a format appropriate
 * for transfer via RPC.
 */
public class Communications : MonoBehaviour {
    public struct Mission {
        public Resource resource;
        public Planet planet;
        public Mission(Resource resource, Planet planet) {
            this.resource = resource;
            this.planet = planet;
        }
        public bool IsInitialized() {
            return (planet != null && resource != null);
        }
    }

    private Mission currentMission = new Mission();

    public Mission GetCurrentMission() {
        return currentMission;
    }

    public void SetCurrentMission(Mission mission) {
        currentMission = mission;
    }

    private Ship currentShip;

    void Start() {
        currentShip = GetComponent<Ship>();
    }

    // Serialize an arbitrary number of arguments as a byte array to send them over an RPC.
    // Unfortunately, NetworkViewIDs can't be serialized, so we send that separately. This means
    // that the 3rd argument must be a NetworkViewID - If you do not need to send one, send null
    // as argument 3 instead.
    public void SendNetworkMessage(string name, IDropTarget target, NetworkViewID networkObjectID, params object[] args) {
        networkView.RPC("RouteMessage", RPCMode.Server, name, target.TargetShip(), target.TargetRole(), networkObjectID, Serializer.SerializeArgs(args));
    }

    // Finds the desired NetworkPlayer using a combination of the ship's NetworkViewID and the
    // targetRole, then sends all the data on to the NetworkPlayer via RPC.
    // This RPC runs on the Server.
    [RPC] 
    private void RouteMessage(string messageName, NetworkViewID targetShipID, int targetRole, NetworkViewID networkObjectID, byte[] args, NetworkMessageInfo info) {
        NetworkView targetShip = NetworkView.Find(targetShipID);
        Ship ship = targetShip.gameObject.GetComponent(typeof(Ship)) as Ship;
        if (ship) {
            NetworkPlayer player = ship.GetPlayerWithRole((CrewStation.Role) targetRole);
            ship.networkView.RPC("ReceiveMessage", player, info.sender, info.networkView.viewID, messageName, networkObjectID, args);
        } else {
            Debug.LogError("Could not find ship with NetworkViewID: " + targetShipID);
        }
    }

    // Calls the named method on the currently active station. Checks for exact method signature match,
    // and then for the exact params with the optional NetworkViewID of the sending ship. 
    // If the active station doesn't implement either method, sends a failure message back to the server.
    // This RPC runs on the Client.
    [RPC]
    private void ReceiveMessage(NetworkPlayer fromPlayer, NetworkViewID fromShip, string messageName, NetworkViewID networkObjectID, byte[] args) {
        CrewStation station = currentShip.GetActiveStation();
        List<object> objArgs = new List<object>(Serializer.DeserializeArgs(args));
        objArgs.Insert(0, networkObjectID);

        MethodInfo method = FindMethod(station, messageName, objArgs);

        // If we don't find a method the first time, check again for a method with NetworkViewID at the end
        if (method == null) {
            objArgs.Add(fromShip);
            method = FindMethod(station, messageName, objArgs);
        }
        
        if (method != null) {
            method.Invoke(station, objArgs.ToArray());
        } else {
            networkView.RPC("MessageFailed", RPCMode.Server, fromPlayer, Network.player, messageName);
        }
    }

    // MessageFailed is called if the chain of message routing failed for any reason. Currently
    // it just prints a Debug.LogError, but it could be used to display on-screen feedback
    // if the user tries to send a piece of information to someone who doesn't care about it.
    // This RPC runs on the Server and the Client
    [RPC]
    private void MessageFailed(NetworkPlayer from, NetworkPlayer to, string messageName) {
        if (Network.isServer) {
            Debug.LogError("Failed to route message: " + messageName + " from " + from.ToString() + " to " + to.ToString());
            networkView.RPC("MessageFailed", from, from, to, messageName);
        } else {
            Debug.LogError("Failed to send message: " + messageName + " to " + to.ToString());
        }
    }

    // Finds a method on a given station, using methodName and the list of arguments.
    // Returns null if the given station has no matching method signature.
    private MethodInfo FindMethod(CrewStation station, string methodName, List<object> args) {
        Type[] types = new Type[args.Count];
        for (int i = 0; i < args.Count; i++) {
            types[i] = args[i].GetType();
        }
        return station.GetType().GetMethod(methodName, types);
    }
}