﻿using UnityEngine;

/*
 * The ShortRangeScanner is used by the Acquisitions Station on a support ship
 * to get more detailed information about a scannable object at close range. It
 * does not cost power to use but has a very limited range, and can find
 * DeepScannable information.
 */
public class ShortRangeScanner : Scanner {

    public override Scannable FindScanTarget() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo)) {
            FindDeepScannable(hitInfo);
            if (scanTarget == null) {
                FindScannable(hitInfo);
            }
        } else {
            scanTarget = null;
        }

        return scanTarget;
    }

    protected void FindDeepScannable(RaycastHit hitInfo) {
        IDeepScannable deepScanTarget = hitInfo.collider.gameObject.GetComponent(typeof(IDeepScannable)) as IDeepScannable;
        if (deepScanTarget != null) {
            this.scanTarget = deepScanTarget.FindDetailedScanTarget(Input.mousePosition);
        }
    }

    protected override float GetFastScanDelay() {
        return 1.0f;
    }

    // Short range scanner uses no energy.
    public override float GetScanCost() {
        return 0.0f;
    }
}