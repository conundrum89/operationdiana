﻿using UnityEngine;

/*
 * The MiningLaser performs mining operations on a celestialBody with mineable sectors.
 * It is controlled by the MiningControlsUI on AcquisitionStation.
 */
public class MiningLaser : MonoBehaviour {
    private CelestialBody currentCelestialBody;
    private Sector currentSector;

    private const int MINING_INCREMENT = 1;

    public Sector GetCurrentSector() {
        return currentSector;
    }

    public void SetCurrentSector(Sector sector) {
        currentSector = sector;
        currentCelestialBody = sector.GetCelestialBody();
    }

    // Attempts to transfer resource from the currentSector to the ship's CargoHold.
    // Returns true if successful, false otherwise.
    public bool TransferResource() {
        Ship ship = GetComponent(typeof(Ship)) as Ship;
        Resource planetResource = currentSector.GetResource();
        if (planetResource.GetAmount() > 0 && ship.GetComponent<CargoHold>().AddResources(planetResource, MINING_INCREMENT)) {
            currentCelestialBody.RemoveResource(currentSector, MINING_INCREMENT);
            return true;
        }
        return false;
    }
}