﻿using UnityEngine;
using System;
using System.Collections;

/*
 * A Scanner provides all scanning related functionality to a ship, including the scanning
 * energy storage and usage logic. The actual logic of finding a scanTarget
 * is left to the child classes to implement.
 */
public abstract class Scanner : MonoBehaviour {
    protected Ship currentShip;

    protected Scannable scanTarget = null;
    private bool isScanning = false;
    // protected IEnumerator currentScan;

    protected float powerLevel = 100;
    protected float currentScanProgress = 0;

    protected const float SCAN_PROGRESS_INCREMENT = 0.1f;
    private const float SLOW_SCAN_MODIFIER = 3.0f;

    void Start() {
        currentShip = GetComponent<Ship>();
    }

    public float GetPowerLevel() {
        return powerLevel;
    }

    public Scannable GetScanTarget() {
        return scanTarget;
    }

    public float GetCurrentScanProgress() {
        return currentScanProgress;
    }

    public bool IsScanning() {
        return isScanning;
    }

    // Begins the scanning process with a calculated cost and delay.
    public void Activate() {
        float scanCost = GetScanCost();
        if (powerLevel > scanCost) {
            StartCoroutine(UpdateScanProgress(GetFastScanDelay(), scanCost));
        } else {
            StartCoroutine(UpdateScanProgress(GetSlowScanDelay()));
        }
    }

    // Increments the scan progress bar and decrements the scanning resource over the 
    // course of the scan.
    private IEnumerator UpdateScanProgress(float scanTime, float scanCost = 0) {
        isScanning = true;

        float scanTimeIncrement = 100.0f * SCAN_PROGRESS_INCREMENT / scanTime;
        float scanCostDecrement = scanCost * SCAN_PROGRESS_INCREMENT / scanTime;

        for (float i = 0; i < scanTime; i += SCAN_PROGRESS_INCREMENT) {
            yield return new WaitForSeconds(SCAN_PROGRESS_INCREMENT);
            currentScanProgress += scanTimeIncrement;
            UseScanningEnergy(scanCostDecrement);
        }

        ScanComplete();
    }

    // Delegates to the scanTarget to actually perform scanning behaviour. Sets the state
    // of the Scanner to have no current scan, and updates the last scanned time on the 
    // scanTarget.
    private void ScanComplete() {
        currentShip.GetActiveStation().GetComponent<ScanningUI>().ScanComplete();
        scanTarget.Scan();
        currentScanProgress = 0;
        isScanning = false;
    }

    private void UseScanningEnergy(float amount) {
        if (powerLevel > amount) {
            powerLevel -= amount;
        } else {
            powerLevel = 0;
        }
    }

    protected void FindScannable(RaycastHit hitInfo) {
        this.scanTarget = hitInfo.collider.gameObject.GetComponent<Scannable>();
    }

    // Defines how the scanner calculates how much power a given scan will cost.
    public abstract float GetScanCost();
    // Defines how the scanner finds a scanTarget from touch input. This method should set the 
    // scanTarget on the scanner, including to null if no scanTarget is clicked.
    public abstract Scannable FindScanTarget();
    // Define how long a scan takes if the station has sufficient power
    protected abstract float GetFastScanDelay();

    // If the station has insufficient power, scanning should take longer.
    protected float GetSlowScanDelay() {
        return GetFastScanDelay() * SLOW_SCAN_MODIFIER;
    }
}