﻿using UnityEngine;
using System.Collections.Generic;

/*
 * The Cargo Hold stores resources and controls how many resources
 * can be added to a ship. It is generic and can be used by support ships
 * and the mothership
 */
public class CargoHold : MonoBehaviour {
    private List<Resource> resources = new List<Resource>();
    public int maxResources;
    
    // Checks if the resource already exists in the cargo hold. If it doesn't, 
    // it checks whether there is room for another resource type in the cargo hold.
    // If there isn't the addition fails.
    public bool AddResources(Resource resource, int amount) {
        Resource shipResource = resources.Find(searchedResource => searchedResource.GetName() == resource.GetName());
        if (shipResource == null && resources.Count <= maxResources) {
            networkView.RPC("AddResource", RPCMode.All, resource.GetName(), amount);
            return true;
        } else if (resources.Count <= maxResources) {
            networkView.RPC("AddResource", RPCMode.All, resource.GetName(), amount);
            return true;
        } else {
            return false;
        }
    }
    
    public List<Resource> GetResources() {
        return resources;
    }
    
    public bool RemoveResources(Resource resource, int amount) {
        Resource shipResource = resources.Find(searchedResource => searchedResource.GetName() == resource.GetName());

        if (shipResource != null) {
            if (shipResource.GetAmount() >= amount) {
                networkView.RPC("RemoveResource", RPCMode.All, resource.GetName(), amount);
                return true;
            } else {
                if (shipResource.GetAmount() > 0) {
                    networkView.RPC("RemoveResource", RPCMode.All, resource.GetName(), shipResource.GetAmount());
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }
    
    public int GetResourceAmount(Resource resource) {
        Resource shipResource = resources.Find(searchedResource => searchedResource.GetName() == resource.GetName());
        if (shipResource != null) {
            return shipResource.GetAmount();
        } else {
            return 0;
        }
    }
    
    [RPC]
    private void RemoveResource(string resourceName, int amount, NetworkMessageInfo info) {
        Resource shipResource = resources.Find(searchedResource => searchedResource.GetName() == resourceName);
        if (shipResource != null) {
            if (shipResource.GetAmount() >= amount) {
                shipResource.RemoveResourceAmount(amount);
            } else if (shipResource.GetAmount() > 0) {
                shipResource.RemoveResourceAmount(shipResource.GetAmount());
            }
            
            if (shipResource.GetAmount() == 0) {
                resources.Remove(shipResource);
            }
        } 
    }
    
    [RPC]
    private void AddResource(string resourceName, int amount, NetworkMessageInfo info) {
        Resource shipResource = resources.Find(searchedResource => searchedResource.GetName() == resourceName);
        if (shipResource == null && resources.Count <= maxResources) {
            resources.Add(Resource.Instantiate(resourceName, amount));
        } else if (resources.Count <= maxResources) {
            shipResource.AddResourceAmount(amount);
        }
    }
    
    public void Reset() {
        networkView.RPC("ResetCargoHold", RPCMode.All);
    }
    
    [RPC]
    private void ResetCargoHold() {
        resources = new List<Resource>();
    }
}