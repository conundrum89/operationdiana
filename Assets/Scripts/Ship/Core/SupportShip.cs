﻿using UnityEngine;
using System.Collections;

/*
 * A support ship represents the craft being piloted by users on iPads.
 * The support ship is a DropTarget to allow the Tactics station to drop
 * missions onto it. All messages coming into a support ship go through 
 * the Communications station.
 */
public class SupportShip : Ship, IDropTarget {
    private const string NAME_PREFIX = "Support Ship R";
    private const int RESPAWN_DELAY = 3;
    private Shield shield;

    public AudioClip destroyedSound;
    public AudioClip respawnSound;

    public static int instanceCount = 0;

    void Start() {
        shield = GetComponent<Shield>();

        // New support ships need to register themselves with the Tactics station
        TacticsStation tactics = NetworkView.FindObjectOfType(typeof(TacticsStation)) as TacticsStation;
        if (tactics) {
            tactics.AddNewSupportShipOverlay(this);
        }
        instanceCount++;
        name = NAME_PREFIX + instanceCount;
    }

    NetworkViewID IDropTarget.TargetShip() {
        return networkView.viewID;
    }

    void IDropTarget.RenderDropTarget() {
        // TODO: Glow effect
    }

    // All external support ship communications should go to the Communications station
    int IDropTarget.TargetRole() {
        return (int)CrewStation.Role.Communications;
    }

    public void Damage() {
        if (shield.Damage() <= 0) {
            Respawn();
        }
    }

    public void Respawn() {
        StartCoroutine(RespawnShip());
    }

    // Disable the ship, and then reset and re-enable it after preset delay
    private IEnumerator RespawnShip() {
        networkView.RPC("DisableShip", RPCMode.All);
        Vector3 resetPosition = (NetworkView.FindObjectOfType(typeof(Mothership)) as Mothership).transform.position;

        yield return new WaitForSeconds(RESPAWN_DELAY);

        networkView.RPC("ResetShip", RPCMode.All, resetPosition);
        networkView.RPC("EnableShip", RPCMode.All);
    }

    [RPC]
    private void ResetShip(Vector3 resetPosition) {
        cargoHold.Reset();
        engine.Reset(resetPosition);
        shield.Reset();
    }

    [RPC]
    private void DisableShip() {
        ShipStateChange(false, destroyedSound);
    }

    [RPC]
    private void EnableShip() {
        ShipStateChange(true, respawnSound);
    }

    private void ShipStateChange(bool state, AudioClip soundEffect) {
        collider.enabled = state;
        CrewStation crewStation = GetActiveStation();
        if (crewStation) {
            crewStation.enabled = state;
            gameObject.GetComponentInChildren<MeshRenderer>().enabled = state;
            crewStation.PlayOverriding(soundEffect);
        }
    }
}
