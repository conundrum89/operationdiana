﻿using UnityEngine;
using System;

/*
 * The LongRangeScanner is used by the Science station on the mothership to scan
 * less detailed information from a scannable object from a longer distance.
 * It takes longer to scan and costs scanner power to perform, and cannot find
 * DeepScannable information.
 */
public class LongRangeScanner : Scanner {

    public override Scannable FindScanTarget() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo)) {
            FindScannable(hitInfo);
        } else {
            scanTarget = null;
        }
        return scanTarget;
    }

    protected override float GetFastScanDelay() {
        return 2.5f;
    }

    // Calculates the cost of the scan based on the distance, using a formula derived
    // from the old static cost points using Wolfram Alpha.
    public override float GetScanCost() {
        Vector3 shipPos = transform.position;
        float distance = Vector3.Distance(shipPos, scanTarget.GetPosition());
        // formula to calculate cost based on distance
        return Mathf.Pow((float) Math.E, (distance * 0.0007f));
    }
}