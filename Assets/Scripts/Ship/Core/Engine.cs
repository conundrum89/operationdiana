﻿using UnityEngine;
using System.Collections;
using System;

/*
 * The Engine is designed to belong to a Ship, providing movement
 * options as well as fuel storage and usage logic where required. The movement
 * details are replicated from clients to the server, and all
 * movement is done on the server itself.
 */
public class Engine : MonoBehaviour {
    public enum Direction { Left, Right };

    public float maxAngularAcceleration = 60f; // Degrees/second^2
    public float maxAngularVelocity = 30f; // Degrees/second
    public float proportionalGain = 20f;
    public float differentialGain = 1f;
    public float fuelUsageRate; // Defined on the ship that has an engine. If set to zero no fuel is needed

    private const float brakeFactor = 0.7f; // Percentage of acceleration allowed to be applied in a non forward direction
    private const float ACCELERATION_RATE = 20f;
    private const float ACCELERATION_RATE_LOW = 5f;
    private const float TURN_INCREMENT = 0.5f;
    private const float TOLERANCE = 0.5f;

    private float currentAcceleration;
    private float desiredSpeed;
    private float desiredHeadingInDegrees;
    private float fuel = 100.0f;
    private float angularAcceleration;
    private float lastError;
    private float angularVelocity = 0;
    private bool isConsumingFuel = false;
    private bool aiEnabled = false;

    void Start() {
        desiredSpeed = 0.0f;
        desiredHeadingInDegrees = 0.0f;
        currentAcceleration = ACCELERATION_RATE;
    }
    
    void FixedUpdate() {
        if (Network.isServer) {
            if (aiEnabled) {
                // TODO: Write this
                // With the new engine system this SHOULD work by updating desiredSpeed and desiredHeadingInDegrees
            }

            UpdateRotation();
            UpdateVelocity();

            if (fuelUsageRate > 0 && isConsumingFuel) {
                UseFuel();
                isConsumingFuel = false;
            }
        }
    }

    /* 
     * This is based on a PID controller (but is a PD controller)
     * http://en.wikipedia.org/wiki/PID_controller
     * The following link was used for reference:
     * http://answers.unity3d.com/questions/197225/pid-controller-simulation.html
     * The reason for this convoluted function is avoid using the inbuilt Unity Torque system but still keep the main functionality
     * Why do we want to avoid the Unity torque system? Quaternion -> EulerAngle is not a reliable method (and Unity uses Quaternions internally)
     * However, EulerAngle -> Quaternion is reliable, so we use that instead (and locally store the relevant details).
     */
    private void UpdateRotation() {
        float currentHeading = transform.eulerAngles.y;

        // Generate the error signal
        float error = desiredHeadingInDegrees - currentHeading;

        // Compensate for the "0 line" (unity requires %360 degrees)
        if (error > 180f) {
            error -= 360f;
        } else if (error < -180f) {
            error += 360f;
        } 

        // if the desired angle is arbitrarily close to the current angle
        if (Mathf.Abs(error) < TURN_INCREMENT) {
            // stick the landing (i.e. compensate for the lack of integral control)
            rigidbody.rotation = Quaternion.Euler(0, desiredHeadingInDegrees % 360, 0);
        }

        // Calculate differential error
        float diff = (error - lastError) / Time.fixedDeltaTime;
        lastError = error;

        // Calculate the acceleration
        angularAcceleration = error * proportionalGain + diff * differentialGain;

        // Limit it to the maximum acceleeration
        angularAcceleration = Mathf.Clamp(angularAcceleration, -maxAngularAcceleration, maxAngularAcceleration);

        // Apply angularAcceleration to the angular velocity
        angularVelocity += angularAcceleration;

        // Limit maximum angularVelocity
        angularVelocity = Mathf.Clamp(angularVelocity, -maxAngularVelocity, maxAngularVelocity);

        currentHeading += angularVelocity * Time.fixedDeltaTime;

        // Apply the rotation by converting from Euler to Quaternion
        rigidbody.rotation = Quaternion.Euler(0, currentHeading % 360, 0);
    }

    private void UpdateVelocity() {
        // In local space calculate the desired and current velocities
        Vector3 desiredVelocity = Vector3.forward * desiredSpeed;
        Vector3 currentVelocity = transform.InverseTransformDirection(rigidbody.velocity);

        Debug.DrawRay(rigidbody.position, transform.TransformDirection(desiredVelocity) * 10, Color.red);
        Debug.DrawRay(rigidbody.position, rigidbody.velocity * 10, Color.green);

        float differenceInMagnitude = (desiredVelocity - currentVelocity).magnitude;

        if (differenceInMagnitude == 0.0f) {
            // As we don't need any more calculations, return out of this function
            return;
        } else if (Mathf.Abs(differenceInMagnitude) < TOLERANCE) {
            // If we are very close to our desired velocity, set it using forces appropriately
            // This prevents us approaching but never reaching it, and thus constantly using fuel.
            rigidbody.AddForce(-currentVelocity, ForceMode.VelocityChange);
            rigidbody.AddForce(desiredVelocity, ForceMode.VelocityChange);

            // As we don't need any more calculations, return out of this function
            return;
        }

        // Figure out the magnitude of the currentVelocity in the forward direction
        float dotProduct = Vector3.Dot(currentVelocity, Vector3.forward);

        // If we need to accelerate forward at all
        if (dotProduct < desiredSpeed) {
            float forwardComponent = desiredSpeed - dotProduct;
            UseMainEngine(forwardComponent);

            float rightComponent = Vector3.Dot(currentVelocity, Vector3.right);
            UseDirectionalEngine(-Vector3.right, rightComponent);
        } else {
            // else we are going too fast!
            Vector3 desiredAcceleration = desiredVelocity - currentVelocity;
            UseDirectionalEngine(desiredAcceleration, desiredAcceleration.magnitude);
        }

    }

    // This is the ship using its main engine (Rocket)
    // Accelerate only applies thrust directly forward
    private void UseMainEngine(float force) {

        rigidbody.AddRelativeForce(Vector3.forward * Mathf.Min(force, currentAcceleration));

        isConsumingFuel = true;
    }

    // This is the ship using its secondary engine (a super powered reaction control system)
    // You can apply this in any direction, but its power is a percentage of the main engine equal to brakeFactor 
    private void UseDirectionalEngine(Vector3 direction, float force) {
        rigidbody.AddRelativeForce(direction.normalized * Mathf.Min(force, currentAcceleration * brakeFactor));

        // Optionally remove fuel useage here (i.e. treat the braking system as a "RCS" that utilises a Bussard (ion) Scoop)
        isConsumingFuel = true;
    }

    private void UseFuel() {
        if (fuel > 0) {
            fuel -= fuelUsageRate;
        } else {
            fuel = 0;
        }

        currentAcceleration = fuel > 0 ? ACCELERATION_RATE : ACCELERATION_RATE_LOW;

        try {
            networkView.RPC("UpdateClientFuel", RPCMode.Others, fuel);
        } catch (UnityException e) {
            // An exception occured. 
            // This occurs because a client was shut down and we could not send the RPC function.
            Debug.Log(e);
        }
    }

    public float GetFuel() {
        return fuel;
    }

    public float GetSpeed() {
        return rigidbody.velocity.magnitude;
    }

    public float GetSpeedInCurrentDirection() {
        Vector3 localVelocity = transform.InverseTransformDirection(rigidbody.velocity);
        return Vector3.Dot(localVelocity, Vector3.forward);
    }
    
    public float GetDesiredSpeed() {
        return desiredSpeed;
    }

    public float GetCurrentHeading() {
        return transform.eulerAngles.y;
    }

    public float GetDesiredHeading() {
        return desiredHeadingInDegrees;
    }
    
    public void Turn(Direction direction) {
        switch (direction) {
            case Direction.Left:
                desiredHeadingInDegrees -= TURN_INCREMENT;
                desiredHeadingInDegrees = (desiredHeadingInDegrees + 360) % 360;
                break;
            case Direction.Right:
                desiredHeadingInDegrees += TURN_INCREMENT;
                desiredHeadingInDegrees = (desiredHeadingInDegrees + 360) % 360;
                break;
        }
        networkView.RPC("SetDesiredHeading", RPCMode.All, desiredHeadingInDegrees);
    }

    public void SetDesiredSpeed(float newDesiredSpeed) {
        desiredSpeed = newDesiredSpeed;
        networkView.RPC("ReceiveSetDesiredSpeedMessage", RPCMode.Server, newDesiredSpeed);
    }
    
    public void Reset(Vector3 resetPosition) {
        desiredSpeed = 0.0f;
        desiredHeadingInDegrees = 0.0f;
        rigidbody.AddForce(-rigidbody.velocity, ForceMode.VelocityChange);
        rigidbody.AddTorque(-rigidbody.angularVelocity, ForceMode.VelocityChange);
        transform.position = resetPosition;
        transform.rotation = Quaternion.identity;
    }

    public bool GetAIState() {
        return aiEnabled;
    }

    public void ToggleAI() {
        aiEnabled = !aiEnabled;

        // Only the server ever performs AI calculations.
        networkView.RPC("ToggleServerAI", RPCMode.Server);
    }

    // Called by: Helm/Navigation Station
    // Called on: Server
    [RPC]
    private void ReceiveSetDesiredSpeedMessage(float newDesiredSpeed) {
        desiredSpeed = newDesiredSpeed;
    }

    // Called by: Helm/Navigation Station
    // Called on: Server
    [RPC]
    private void SetDesiredHeading(float newDesiredHeading) {
        desiredHeadingInDegrees = newDesiredHeading;
    }

    // Called by: Helm/Navigation Station
    // Called on: Server
    [RPC]
    private void ToggleServerAI() {
        aiEnabled = !aiEnabled;
    }

    // Called by: Server
    // Called on: Others
    [RPC]
    private void UpdateClientFuel(float newFuel) {
        fuel = newFuel;
        currentAcceleration = fuel > 0 ? ACCELERATION_RATE : ACCELERATION_RATE_LOW;
    }
}