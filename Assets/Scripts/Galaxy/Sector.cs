﻿using UnityEngine;
using System.Collections.Generic;

/*
 * The Sector class is designed to be used by the Planet class to store
 * specific information about resources and habitation on specific sectors
 * of the Planet.
 */
﻿public class Sector : MonoBehaviour {

     private Resource minableResource = null;
     private CelestialBody celestialBody;
     private SectorScannable scannable;

     void Start() {
         celestialBody = GetComponent<CelestialBody>();
         scannable = gameObject.AddComponent<SectorScannable>();
         scannable.SetSector(this);
     }

     public void GenerateRandomResources(int optionalUpperLimit = 0) {
         if (!Network.isServer) {
             throw new UnityException("Resources should only be generated server side");
         }

         int resourceAmount;
         if (optionalUpperLimit != 0) {
             resourceAmount = Random.Range(0, optionalUpperLimit);
         } else {
             resourceAmount = Random.Range(Resource.LOW_SUPPLY / 2, (Resource.HIGH_SUPPLY + Resource.LOW_SUPPLY) / 2);
         }
         
         minableResource = Resource.InstantiateRandom(resourceAmount);
     }

     public Resource GetResource() {
         return minableResource;
     }

     public void SetResource(Resource resource) {
         minableResource = resource;
     }

     public bool IsDepleted() {
         return minableResource != null && minableResource.GetAmount() <= 0;
     }

     public void SetCelestialBody(CelestialBody celestialBody) {
         this.celestialBody = celestialBody;
     }

     public CelestialBody GetCelestialBody() {
         return celestialBody;
     }

     public SectorScannable GetScannable() {
         return scannable;
     }

     public object[] ToArray() {
         return new object[] { minableResource.GetName(), minableResource.GetAmount() };
     }
 }