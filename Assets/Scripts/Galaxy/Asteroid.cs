﻿using UnityEngine;
using System.Collections;

/*
 * Asteroid class extends the CelestialBody class with functionality defining
 * additional interactions during collision events and mining interactions.
 */
public class Asteroid : CelestialBody {

    private AsteroidField owner;

    private const int ASTEROID_SECTOR_COUNT = 1;

    protected override void ShipCollision(Ship ship, Collision other) {
        Explode();

        if (ship is SupportShip) {
            (ship as SupportShip).Damage();
        }
    }

    void Update() {
        if (sectors.Count == 0) {
            GenerateSectors(ASTEROID_SECTOR_COUNT);
        } else if (sectors[0].IsDepleted()) {
            Explode();
        }
    }

    public void SetupSectors() {
        GenerateSectorsWithResources(ASTEROID_SECTOR_COUNT, Resource.LOW_SUPPLY);
    }

    private void Explode() {
        // The asteroid plays its own sound on destruction it cannot use the audio playing pattern 
        // found in the rest of the code as it is not tied to a clients input directly.
        AudioSource soundSource = gameObject.AddComponent<AudioSource>();
        soundSource.minDistance = 5000f;
        soundSource.PlayOneShot((Resources.Load("Galaxy/AsteroidExplosion")) as AudioClip);

        renderer.enabled = false;
        // Cleanup of the object only needs a single Destroy call
        if (Network.isServer) {
            StartCoroutine(WaitThenDestroy()); 
        }
    }

    private IEnumerator WaitThenDestroy() {
        yield return new WaitForSeconds(.5f);
        Network.RemoveRPCs(networkView.viewID);
        Network.Destroy(networkView.viewID);
    }

}
