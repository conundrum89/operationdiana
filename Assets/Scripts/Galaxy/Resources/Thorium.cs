﻿/*
 * Thorium is a resource used for X
 */
public class Thorium : Resource {
    public override string GetName() {
        return "Thorium";
    }

    public Thorium(int amount) : base(amount) { }
}