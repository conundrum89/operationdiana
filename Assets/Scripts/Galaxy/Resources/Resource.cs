﻿using System;

/*
 * The Resource class stores the generic information about resource types, such as the amount.
 * It also provides static methods to instantiate Resources of various types.
 */
public abstract class Resource {
    // Amount will be in a certain metric, i.e. KG, Tons etc
    protected int amount;
    public static string[] RESOURCE_TYPES = { "Phostolite", "Thorium", "Adamantite", "Plutonium" };

    // Below LOW_SUPPLY is considered low.
    public const int LOW_SUPPLY = 800;
    // Between LOW_SUPPLY and HIGH_SUPPLY is considered medium.
    public const int HIGH_SUPPLY = 2000;
    // Above HIGH_SUPPLY is considered high. 

    public Resource() {
        this.amount = 0;
    }

    public Resource(int amount) {
        this.amount = amount;
    }

    public int GetAmount() {
        return amount;
    }

    public abstract string GetName();

    public void RemoveResourceAmount(int amount) {
        this.amount -= amount;
    }

    public void AddResourceAmount(int amount) {
        this.amount += amount;
    }

    // Instantiates a random Resource type with the specified amount.
    public static Resource InstantiateRandom(int amount) {
        string resourceType = RESOURCE_TYPES[UnityEngine.Random.Range(0, RESOURCE_TYPES.Length - 1)];
        return Instantiate(resourceType, amount);
    }

    // Creates an instance of the specified Resource type with the specified amount.
    // This is used when the resource type is sent via an RPC as a string.
    public static Resource Instantiate(string type, int amount) {
        return Activator.CreateInstance(Type.GetType(type), amount) as Resource;
    }

    public static Resource Instantiate(object[] details) {
        return Instantiate((string) details[0], (int) details[1]);
    }
}