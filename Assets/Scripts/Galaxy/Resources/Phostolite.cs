﻿/*
 * Phostolite is a resource used for X
 */
public class Phostolite : Resource {
    public override string GetName() {
        return "Phostolite";
    }

    public Phostolite(int amount) : base(amount) { }
}