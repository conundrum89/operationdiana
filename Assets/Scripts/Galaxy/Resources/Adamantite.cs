﻿/*
 * Adamantite is a resource used for X
 */
public class Adamantite : Resource {
    public override string GetName() {
        return "Adamantite";
    }

    public Adamantite(int amount) : base(amount) { }
}