﻿/*
 * Plutonium is a resource used for X
 */
public class Plutonium : Resource {
    public override string GetName() {
        return "Plutonium";
    }

    public Plutonium(int amount) : base(amount) { }
}