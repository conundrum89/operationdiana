﻿using UnityEngine;
using System.Collections.Generic;
/*
 * This class generates a set and sets up an defined number of asteroids around its 
 * position in the scene.
 */
public class AsteroidField : MonoBehaviour {

    public Asteroid[] asteroidPrefabs;
    public int asteroidCount;
    private float fieldSize = 700f;

    void OnServerInitialized() {
        Generate();
    }

    public void Generate() {
        for (int i = 0; i < asteroidCount; i++) {
            Asteroid randomAsteroidPrefab = asteroidPrefabs[Random.Range(0, asteroidPrefabs.Length - 1)];
            Asteroid newAsteroid = (Asteroid)Network.Instantiate(randomAsteroidPrefab, GenerateSpawnPoint(), Random.rotation, 0);
            newAsteroid.SetupSectors();
        }
    }

    private Vector3 GenerateSpawnPoint() {
        Vector2 target = Random.insideUnitCircle * fieldSize;
        return transform.position + new Vector3(target.x, 0, target.y);
    }
}
