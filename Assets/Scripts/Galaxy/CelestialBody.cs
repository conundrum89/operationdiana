﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

/*
 * The CelestialBody class contains all of the details that
 * a CelestialBody holds, providing information for other components
 * about the details of the planet, as well as sectors and resources.
 * CelestialBodies on clients will automatically query the server to get the details
 * that they require.
 */
public class CelestialBody : MonoBehaviour {
    protected static int SECTORS_PER_CELESTIAL_BODY = 4;
    protected CelestialBodyScannable scannable;

    protected List<Sector> sectors;

    // TODO: Get rid of this during the LineRender refactor.
    protected GameObject secondLineObject;

    //This value can be tweaked so that the result of collision on ships movement feels better
    private const float RELFECTION_MAGNITUDE = 150.0F;

    void Awake() {
        scannable = GetComponent<CelestialBodyScannable>();
        sectors = new List<Sector>();

        if (!collider || !rigidbody || !rigidbody.isKinematic) {
            throw new UnityException("Celestial body: " + name + " has improper configuration for collision detection");
        }
    }

    void Update() {
        Vector3 rotateVector = Vector3.up * Time.deltaTime;

        if (scannable.rotationPeriod != 0) {
            // Convert the rotation period so that a higher hour value equals a slower rotation
            rotateVector = rotateVector / (scannable.rotationPeriod / 10000.0f);
        }

        transform.Rotate(rotateVector);
    }

    void OnCollisionEnter(Collision other) {
        if (Network.isServer && other.gameObject.GetComponent<Ship>()) {
            ShipCollision(other.gameObject.GetComponent<Ship>(), other);
        }
    }

    /*This contains the default behaviour approximating an inelastic collision between ships. 
     * and celestial bodies. Any child Celestial bodies should override this to define
     * additional interactions with ships during collisions.
    */
    protected virtual void ShipCollision(Ship ship, Collision other) {
        //Modifying the ship's rigidbody vector 
        Rigidbody shipRigidbody = ship.rigidbody;
        shipRigidbody.velocity = Vector3.zero;

        Vector3 contactPointNormal = other.contacts[0].point.normalized;
        Vector3 reflectionVector = Vector3.Reflect(shipRigidbody.velocity, contactPointNormal);
        shipRigidbody.AddForce(reflectionVector * RELFECTION_MAGNITUDE, ForceMode.Acceleration);

        if (ship is SupportShip) {
            (ship as SupportShip).Respawn();
        }
    }

    //Generate sectors for this celestial body 
    protected void GenerateSectors(int sectorCount) {
        CreateSectors(sectorCount);
    }

    //Generate sectors for this celestial body with resources
    protected void GenerateSectorsWithResources(int sectorCount, int resourceAmount = 0) {
        CreateSectors(sectorCount);
        for (int i = 0; i < sectorCount; i++) {
            sectors[i].GenerateRandomResources(resourceAmount);
        }
    }

    private void CreateSectors(int count) {
        for (int i = 0; i < count; i++) {
            Sector newSector = gameObject.AddComponent<Sector>();
            sectors.Add(newSector);
        }
    }

    public void RemoveResource(Sector sector, int amount) {
        networkView.RPC("RemoveResourceRPC", RPCMode.All, sectors.IndexOf(sector), amount);
    }

    public List<Sector> GetSectors() {
        return sectors;
    }

    // Calculates the amount of a given resource available on the CelestialBody,
    // and returns a string denoting the supply as None, Low, Medium or High.
    public string GetResourceSupply(string resourceType) {
        int resourceAmount = 0;
        Resource resource;
        foreach (Sector sector in sectors) {
            if ((resource = sector.GetResource()) != null && resource.GetName() == resourceType) {
                resourceAmount += resource.GetAmount();
            }
        }
        if (resourceAmount == 0) {
            return "None";
        } else if (resourceAmount < Resource.LOW_SUPPLY) {
            return "Low";
        } else if (resourceAmount < Resource.HIGH_SUPPLY) {
            return "Medium";
        } else {
            return "High";
        }
    }

    // This RPC runs on the server
    [RPC]
    protected void RemoveResourceRPC(int sectorIndex, int amount) {
        Sector sector = sectors[sectorIndex];
        if (sector.GetScannable().IsScanned() || Network.isServer) {
            sectors[sectorIndex].GetResource().RemoveResourceAmount(amount);
        }
    }
}
