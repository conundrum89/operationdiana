﻿using UnityEngine;
using System.Collections.Generic;

/*
 * The System Map Generator generates a random system map for use in testing.
 * This will eventually be replaced by a preset system.
 */
public class PlanetPrefabGenerator : MonoBehaviour {
    //TODO: Create a list of all our crafted planets and use those to generate the system. System size shouldn't be required
    public Transform star;
    public Transform[] planetPrefabs;
    public float systemSize;
    public float planetGap;
    private List<Planet> planets = new List<Planet>();
    private const int MAX_PLANETS = 25;

    public const int HEIGHT_OFFSET = -80;

    private const int SMALL_SCALE = 75;
    private const int EARTH_SCALE = 125;
    private const int MED_SCALE = 175;
    private const int LARGE_SCALE = 300;

    void Awake(){
        if (planetPrefabs.Length == 0) {
            Debug.LogError("planetPrefabs empty!");
        }
    }
    
    void Update() {
        if (Input.GetKeyDown(KeyCode.G) && Network.isServer) {
            GenerateSystem();
        }

        if (Input.GetKeyDown(KeyCode.C) && Network.isServer) {
            DestroyPlanets();
        }
    }

    private Vector3 GeneratePlanetSpawnPoint() {
        Vector2 target = Random.insideUnitCircle * systemSize;
        bool invalidPoint = false;
        int counter = 0;

        do {
            target = Random.insideUnitCircle * systemSize;
            invalidPoint = false;

            if (Vector3.Distance(star.position, target) <= (planetGap + star.transform.localScale.x)
            || Vector3.Distance(star.position, target) <= (planetGap + star.transform.localScale.y)) {
                invalidPoint = true;
            }

            if (invalidPoint == false) {
                foreach (Planet planet in planets) {
                    if (Vector2.Distance(new Vector2(planet.transform.localScale.x + planet.transform.position.x, planet.transform.localScale.z + planet.transform.position.z), target) <= planetGap) {
                        invalidPoint = true;
                        break;
                    }
                }
            }
            
            // This is here to prevent an infinite loop should there be not enough room for all the planets,
            // if 1000 points are unsuccessfully generated, there is not enough room in the system, either the gap size needs to be reduced or system size increased
            counter++;
            if (counter >= 1000) {
                Debug.LogWarning("Planet " + planets.Count + ", Generated 1000 points unsuccessfully, accepting point: " + target.x + ", " + target.y);
                invalidPoint = false;
            }
        } while (invalidPoint == true);

        return new Vector3(target.x, HEIGHT_OFFSET, target.y);
    }
    
    private Planet GetRandomPlanet() {
        // int planetType = 0;
        Transform planetTransform;
        Planet newPlanet;

        // Pick random prefab with random spawn point
        planetTransform = (Transform) Network.Instantiate(planetPrefabs[Random.Range(0, 4)], GeneratePlanetSpawnPoint(), new Quaternion(), 0);
        newPlanet = planetTransform.gameObject.GetComponent<Planet>();
        newPlanet.transform.TransformPoint(planetTransform.position);
        
        // Generate a random scale for the planets.
        int planetScale = Random.Range(75, 300);
        newPlanet.SetPlanetProperties(planetScale);
        newPlanet.transform.localScale = planetTransform.localScale * newPlanet.scale;

        newPlanet.transform.name = newPlanet.name;
        
        return newPlanet;
    }

    private void GenerateSystem() {
        // Get all planets, include any prefabs in the scene
        planets = new List<Planet>(FindObjectsOfType(typeof(Planet)) as Planet[]);

        while (planets.Count < MAX_PLANETS) {
            planets.Add(GetRandomPlanet());
        }
    }

    private void DestroyPlanets() {
        planets = new List<Planet>(FindObjectsOfType(typeof(Planet)) as Planet[]);
        
        foreach (Planet planet in planets) {
            // All previous RPC calls must be deleted to stop buffered create calls from propogating to newly connected clients
            Network.RemoveRPCs(((NetworkView) planet.gameObject.GetComponent("NetworkView")).viewID);
            Network.Destroy(planet.gameObject);
        }
        planets.Clear();
    }
}