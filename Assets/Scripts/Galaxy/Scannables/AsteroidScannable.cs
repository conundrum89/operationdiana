﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/*
 * Defines how asteroids should render when deep scanned for mining purposes.
 */
public class AsteroidScannable : CelestialBodyScannable, IDeepScannable {

    public Scannable FindDetailedScanTarget(Vector3 mousePosition) {
        //This code assumes that asteroids only ever have one resource
        return celestialBody.GetSectors()[0].GetScannable();
    }
}
