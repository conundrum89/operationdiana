﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;

/*
 * Defines how CelestialBodies should render when scanned.
 */
public class CelestialBodyScannable : Scannable {

    protected CelestialBody celestialBody;

    // Mass of the celestialBody... displayed as "(mass) x 10^23 kg"
    public float mass;
    // Radius of the celestialBody in km.
    public int radius;
    // Rotation period (length of day) in hours.
    public int rotationPeriod;

    void Start() {
        celestialBody = GetComponent<CelestialBody>();
    }

    public override void RenderDetail() {
        GUILayout.Label(GetDescription());
    }

    private string GetDescription() {
        StringBuilder sb = new StringBuilder();
        sb.Append("Mass: ").Append(mass).Append(" x 10^24kg\n")
            .Append("Radius: ").Append(radius).Append(" km\n")
            .Append("Rotation Period: ").Append(rotationPeriod).Append(" hours\n");
        return sb.ToString();
    }

    public override void Scan() {
        base.Scan();
        networkView.RPC("RequestCelestialBodyDetails", RPCMode.Server);
    }

    public override List<UIComponent> GetSubUIComponents(UIComponent parent) {
        if (parent.GetOwnerStation().GetType() == typeof(TacticsStation)) {
            return GetDraggableResourceComponents(parent);
        } else if (parent.GetOwnerStation().GetType() == typeof(ScienceStation)) {
            return GetDraggableCelestialBodyComponent(parent);
        }

        return new List<UIComponent>();
    }

    // If Science is scanning, then the entire CelestialBody component needs to be draggable to allow for 
    // information transfer.
    private List<UIComponent> GetDraggableCelestialBodyComponent(UIComponent parent) {
        List<UIComponent> subUIComponents = new List<UIComponent>();

        CelestialBodyDetailUI celestialBodyDetail = parent.GetOwnerStation().AddSubUIComponent(typeof(CelestialBodyDetailUI)) as CelestialBodyDetailUI;
        celestialBodyDetail.InitializeWindow(1, 2, 40, 96, 60, parent.GetBoundingRect());
        celestialBodyDetail.SetCelestialBody(celestialBody);
        subUIComponents.Add(celestialBodyDetail);

        return subUIComponents;
    }

    // If Tactics is scanning, the Resource components need to be Draggable to allow for mission allocation.
    private List<UIComponent> GetDraggableResourceComponents(UIComponent parent) {
        List<UIComponent> subUIComponents = new List<UIComponent>();

        for (int i = 0; i < Resource.RESOURCE_TYPES.Length; i++) {
            ResourceDetailUI resourceDetail = parent.GetOwnerStation().AddSubUIComponent(typeof(ResourceDetailUI)) as ResourceDetailUI;

            // Calculate the top position for this window
            float top = 40 + (15 * i);
            resourceDetail.InitializeWindow(i, 2, top, 96, 15, parent.GetBoundingRect());
            resourceDetail.SetResource(Resource.RESOURCE_TYPES[i]);
            resourceDetail.SetCelestialBody(celestialBody);
            subUIComponents.Add(resourceDetail);
        }

        return subUIComponents;
    }

    [RPC]
    public void RequestCelestialBodyDetails(NetworkMessageInfo info) {
        List<object[]> sectorDetails = new List<object[]>();
        
        foreach (Sector sector in celestialBody.GetSectors()) {
            sectorDetails.Add(sector.ToArray());
        }

        byte[] byteSectorDetails = Serializer.SerializeArgs(sectorDetails);

        networkView.RPC("ReceiveCelestialBodyDetails", info.sender,
            mass,
            radius,
            rotationPeriod,
            byteSectorDetails);
    }

    [RPC]
    public void ReceiveCelestialBodyDetails(float mass, int radius, int rotationPeriod, byte[] byteSectorDetails) {
        this.mass = mass;
        this.radius = radius;
        this.rotationPeriod = rotationPeriod;

        List<object[]> sectorDetails = (List<object[]>)Serializer.DeserializeArgs(byteSectorDetails)[0];
        List<Sector> sectors = celestialBody.GetSectors();

        //The first time the details are received Sector components need to be created to house
        //the information
        if (sectors.Count == 0) {
            for (int i = 0; i < sectorDetails.Count; i++) {
                Sector newSector = celestialBody.gameObject.AddComponent<Sector>();
                sectors.Add(newSector);
            }
        }

        //Update the sector components with the resource information
        foreach (object[] resourceDetail in sectorDetails) {
            Resource resource = Resource.Instantiate(resourceDetail);
            sectors[sectorDetails.IndexOf(resourceDetail)].SetResource(resource);
        }
    }
}
