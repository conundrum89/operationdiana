﻿using UnityEngine;
using System.Collections.Generic;


/*
 * Defines how sectors should render when scanned.
 */
public class SectorScannable : Scannable {
    private CelestialBody celestialBody;
    private Sector sector;

    void Start() {
        celestialBody = GetComponent<CelestialBody>();
    }

    public override void Scan() {
        base.Scan();
        networkView.RPC("RequestSectorDetails", RPCMode.Server, celestialBody.GetSectors().IndexOf(sector));
    }

    // Sectors can only be scanned from orbit.
    public override bool CanBeScannedBy(Ship ship) {
        Vector3 shipPos = ship.transform.position;
        float distance = Vector3.Distance(shipPos, GetPosition());
        // TODO: Update to use some kind of orbit distance
        return distance < 250;
    }

    public override void RenderDetail() { }

    public override List<UIComponent> GetSubUIComponents(UIComponent parent) {
        List<UIComponent> subUIComponents = new List<UIComponent>();
        MiningControlsUI miningControls = parent.GetOwnerStation().AddSubUIComponent(typeof(MiningControlsUI)) as MiningControlsUI;
        miningControls.SetBounds(2, 21, 96, 77, parent.GetBoundingRect());
        miningControls.SetCurrentSector(sector);
        subUIComponents.Add(miningControls);
        return subUIComponents;
    }

    public override string WindowHeading() {
        return celestialBody.name + ": Sector " + (celestialBody.GetSectors().IndexOf(sector) + 1);
    }

    public void SetSector(Sector sector) {
        this.sector = sector;
    }

    // This RPC represents a request for the details of a single sector, and returns the
    // details to the player who requested them.
    // This RPC runs on the server.
    [RPC]
    protected void RequestSectorDetails(int sectorIndex, NetworkMessageInfo info) {
        Resource resource = celestialBody.GetSectors()[sectorIndex].GetResource();
        networkView.RPC("ReceiveSectorDetails", info.sender, sectorIndex, resource.GetName(), resource.GetAmount());
    }

    // The client receives the details for a single sector here.
    // This RPC runs on the client.
    [RPC]
    protected void ReceiveSectorDetails(int sectorIndex, string resourceType, int resourceAmount) {
        Resource resource = Resource.Instantiate(resourceType, resourceAmount);
        celestialBody.GetSectors()[sectorIndex].SetResource(resource);
    }
}