﻿using UnityEngine;

/*
 * Defines how planet celestial bodies should render when deep scanned.
 */
public class PlanetScannable : CelestialBodyScannable, IDeepScannable {
    
    Scannable IDeepScannable.FindDetailedScanTarget(Vector3 mousePosition) {
        Vector3 planetPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (mousePosition.x < planetPosition.x && mousePosition.y >= planetPosition.y) {
            return celestialBody.GetSectors()[0].GetScannable();
        } else if (mousePosition.x >= planetPosition.x && mousePosition.y >= planetPosition.y) {
            return celestialBody.GetSectors()[1].GetScannable();
        } else if (mousePosition.x < planetPosition.x && mousePosition.y < planetPosition.y) {
            return celestialBody.GetSectors()[2].GetScannable();
        } else {
            return celestialBody.GetSectors()[3].GetScannable();
        }
    }
}