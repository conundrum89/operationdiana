﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

/*
 * Scannable is the base class for all scannable components. Any object with a 
 * Scannable component attached is treated as a valid scanning target.
 */
public abstract class Scannable : MonoBehaviour {
    protected DateTime? lastScanned = null;

    // Different scripts on the same object cannot share the same RPC names. Therefore, each 
    // subclass of Scannable must implement its own Scan() method. They should call base.Scan()
    // to ensure the lastScanned variable is set.
    public virtual void Scan() {
        SetLastScanned(DateTime.Now);
    }

    public abstract void RenderDetail();

    public DateTime? GetLastScanned() {
        return lastScanned;
    }

    public void SetLastScanned(DateTime? lastScanned) {
        this.lastScanned = lastScanned;
    }

    public bool IsScanned() {
        return (lastScanned != null);
    }

    public virtual bool CanBeScannedBy(Ship ship) {
        return true;
    }

    public Vector3 GetPosition() {
        return transform.position;
    }

    public virtual List<UIComponent> GetSubUIComponents(UIComponent parent) {
        return new List<UIComponent>();
    }

    // Renders the name of the targeted object, and how long ago it was last scanned
    public virtual string WindowHeading() {
        StringBuilder sb = new StringBuilder();
        sb.Append(name);
        
        if (IsScanned()) {
            sb.Append("\t\t");
            DateTime now = DateTime.Now;
            TimeSpan span = now.Subtract((DateTime) lastScanned);
            int seconds = (int) Mathf.Floor((float) span.TotalSeconds % 60);
            int minutes = (int) Mathf.Floor((float) span.TotalMinutes);
            sb.Append("Last scanned ").Append(minutes).Append(" minutes and ").Append(seconds).Append(" seconds ago.");
        }

        return sb.ToString();
    }
}