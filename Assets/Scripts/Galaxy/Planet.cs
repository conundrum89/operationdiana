using UnityEngine;
using System.Collections.Generic;
using System.Text;

/*
 * Planet class extends the CelestialBody class with additional data and functionality
 * pertaining to collision interactions with ships.
 */
public class Planet : CelestialBody {

    public int scale;

    private const int PLANET_SECTOR_COUNT = 4;

    public void SetPlanetProperties(int scale) {
        scannable.mass = this.scale = scale;
        scannable.radius = scale * 150;
        
        // Randomly set the other planet properties
        scannable.rotationPeriod = Random.Range(8, 5000);
        this.name = "X" + Random.Range(1, 9) + "D" + Random.Range(1, 250) + "Z" + Random.Range(1, 150);
    }

    void OnServerInitialized() {
        GenerateSectorsWithResources(PLANET_SECTOR_COUNT);
    }

    void OnConnectedToServer() {
        GenerateSectors(PLANET_SECTOR_COUNT);
    }
}
